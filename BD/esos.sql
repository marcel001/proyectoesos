--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ciudad; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE ciudad (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE ciudad OWNER TO root;

--
-- Name: ciudad_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ciudad_id_seq OWNER TO root;

--
-- Name: ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE ciudad_id_seq OWNED BY ciudad.id;


--
-- Name: datoscontacto; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE datoscontacto (
    id integer NOT NULL,
    rut character varying(12) NOT NULL,
    celular character varying(12) NOT NULL,
    nombres character varying(150) NOT NULL,
    apellidos character varying(150) NOT NULL,
    cargo character varying(150) NOT NULL,
    correo character varying(150),
    pass character varying(60) NOT NULL
);


ALTER TABLE datoscontacto OWNER TO root;

--
-- Name: datoscontacto_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE datoscontacto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE datoscontacto_id_seq OWNER TO root;

--
-- Name: datoscontacto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE datoscontacto_id_seq OWNED BY datoscontacto.id;


--
-- Name: etiqueta; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE etiqueta (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE etiqueta OWNER TO root;

--
-- Name: etiqueta_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE etiqueta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE etiqueta_id_seq OWNER TO root;

--
-- Name: etiqueta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE etiqueta_id_seq OWNED BY etiqueta.id;


--
-- Name: etiqueta_multimediadron; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE etiqueta_multimediadron (
    etiquetaid integer NOT NULL,
    multimediadronid integer NOT NULL
);


ALTER TABLE etiqueta_multimediadron OWNER TO root;

--
-- Name: mensaje; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE mensaje (
    id integer NOT NULL,
    fecha timestamp without time zone NOT NULL,
    latitud character varying(20),
    longitud character varying(20),
    mensaje character varying(255),
    datoscontactoid integer NOT NULL,
    tipomultimediaid integer NOT NULL
);


ALTER TABLE mensaje OWNER TO root;

--
-- Name: mensaje_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE mensaje_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mensaje_id_seq OWNER TO root;

--
-- Name: mensaje_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE mensaje_id_seq OWNED BY mensaje.id;


--
-- Name: mensajemultimedia; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE mensajemultimedia (
    id integer NOT NULL,
    url character varying(255),
    fecha timestamp without time zone NOT NULL,
    mensajeid integer NOT NULL
);


ALTER TABLE mensajemultimedia OWNER TO root;

--
-- Name: mensajemultimedia_etiqueta; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE mensajemultimedia_etiqueta (
    mensajemultimediaid integer NOT NULL,
    etiquetaid integer NOT NULL
);


ALTER TABLE mensajemultimedia_etiqueta OWNER TO root;

--
-- Name: mensajemultimedia_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE mensajemultimedia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mensajemultimedia_id_seq OWNER TO root;

--
-- Name: mensajemultimedia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE mensajemultimedia_id_seq OWNED BY mensajemultimedia.id;


--
-- Name: multimediadron; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE multimediadron (
    id integer NOT NULL,
    altitud double precision,
    longitud character varying(20),
    latitud character varying(20),
    url character varying(255) NOT NULL,
    urlreducida character varying(255),
    fecha timestamp without time zone NOT NULL,
    tipomultimediaid integer NOT NULL,
    ciudadid integer NOT NULL
);


ALTER TABLE multimediadron OWNER TO root;

--
-- Name: multimediadron_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE multimediadron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE multimediadron_id_seq OWNER TO root;

--
-- Name: multimediadron_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE multimediadron_id_seq OWNED BY multimediadron.id;


--
-- Name: streamingdron; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE streamingdron (
    id integer NOT NULL,
    url character varying(255) NOT NULL,
    fecha timestamp without time zone NOT NULL,
    tipomultimediaid integer NOT NULL
);


ALTER TABLE streamingdron OWNER TO root;

--
-- Name: streamingdron_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE streamingdron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE streamingdron_id_seq OWNER TO root;

--
-- Name: streamingdron_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE streamingdron_id_seq OWNED BY streamingdron.id;


--
-- Name: tipomultimedia; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE tipomultimedia (
    id integer NOT NULL,
    tipo character varying(50) NOT NULL
);


ALTER TABLE tipomultimedia OWNER TO root;

--
-- Name: tipomultimedia_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE tipomultimedia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipomultimedia_id_seq OWNER TO root;

--
-- Name: tipomultimedia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE tipomultimedia_id_seq OWNED BY tipomultimedia.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE usuario (
    id integer NOT NULL,
    nick character varying(100) NOT NULL,
    pass character varying(100) NOT NULL
);


ALTER TABLE usuario OWNER TO root;

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_id_seq OWNER TO root;

--
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: root
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- Name: ciudad id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY ciudad ALTER COLUMN id SET DEFAULT nextval('ciudad_id_seq'::regclass);


--
-- Name: datoscontacto id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY datoscontacto ALTER COLUMN id SET DEFAULT nextval('datoscontacto_id_seq'::regclass);


--
-- Name: etiqueta id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY etiqueta ALTER COLUMN id SET DEFAULT nextval('etiqueta_id_seq'::regclass);


--
-- Name: mensaje id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensaje ALTER COLUMN id SET DEFAULT nextval('mensaje_id_seq'::regclass);


--
-- Name: mensajemultimedia id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia ALTER COLUMN id SET DEFAULT nextval('mensajemultimedia_id_seq'::regclass);


--
-- Name: multimediadron id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY multimediadron ALTER COLUMN id SET DEFAULT nextval('multimediadron_id_seq'::regclass);


--
-- Name: streamingdron id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY streamingdron ALTER COLUMN id SET DEFAULT nextval('streamingdron_id_seq'::regclass);


--
-- Name: tipomultimedia id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY tipomultimedia ALTER COLUMN id SET DEFAULT nextval('tipomultimedia_id_seq'::regclass);


--
-- Name: usuario id; Type: DEFAULT; Schema: public; Owner: root
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: root
--

COPY ciudad (id, nombre) FROM stdin;
1	Arica
2	Antofagasta
3	Santiago
4	Temuco
\.


--
-- Name: ciudad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('ciudad_id_seq', 1, false);


--
-- Data for Name: datoscontacto; Type: TABLE DATA; Schema: public; Owner: root
--

COPY datoscontacto (id, rut, celular, nombres, apellidos, cargo, correo, pass) FROM stdin;
\.


--
-- Name: datoscontacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('datoscontacto_id_seq', 1, false);


--
-- Data for Name: etiqueta; Type: TABLE DATA; Schema: public; Owner: root
--

COPY etiqueta (id, nombre) FROM stdin;
\.


--
-- Name: etiqueta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('etiqueta_id_seq', 1, false);


--
-- Data for Name: etiqueta_multimediadron; Type: TABLE DATA; Schema: public; Owner: root
--

COPY etiqueta_multimediadron (etiquetaid, multimediadronid) FROM stdin;
\.


--
-- Data for Name: mensaje; Type: TABLE DATA; Schema: public; Owner: root
--

COPY mensaje (id, fecha, latitud, longitud, mensaje, datoscontactoid, tipomultimediaid) FROM stdin;
\.


--
-- Name: mensaje_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('mensaje_id_seq', 1, false);


--
-- Data for Name: mensajemultimedia; Type: TABLE DATA; Schema: public; Owner: root
--

COPY mensajemultimedia (id, url, fecha, mensajeid) FROM stdin;
\.


--
-- Data for Name: mensajemultimedia_etiqueta; Type: TABLE DATA; Schema: public; Owner: root
--

COPY mensajemultimedia_etiqueta (mensajemultimediaid, etiquetaid) FROM stdin;
\.


--
-- Name: mensajemultimedia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('mensajemultimedia_id_seq', 1, false);


--
-- Data for Name: multimediadron; Type: TABLE DATA; Schema: public; Owner: root
--

COPY multimediadron (id, altitud, longitud, latitud, url, urlreducida, fecha, tipomultimediaid, ciudadid) FROM stdin;
1	1	1	1	dashboard/imagenes/foto1.jpg	foto1.jpg	2016-11-21 09:31:52.076445	2	3
2	1	1	1	dashboard/videos/cerati1.mp4	foto1.jpg	2016-11-21 09:55:17.538017	3	3
\.


--
-- Name: multimediadron_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('multimediadron_id_seq', 1, false);


--
-- Data for Name: streamingdron; Type: TABLE DATA; Schema: public; Owner: root
--

COPY streamingdron (id, url, fecha, tipomultimediaid) FROM stdin;
\.


--
-- Name: streamingdron_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('streamingdron_id_seq', 1, false);


--
-- Data for Name: tipomultimedia; Type: TABLE DATA; Schema: public; Owner: root
--

COPY tipomultimedia (id, tipo) FROM stdin;
1	Audio
2	Imagen
3	Video
\.


--
-- Name: tipomultimedia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('tipomultimedia_id_seq', 1, false);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: root
--

COPY usuario (id, nick, pass) FROM stdin;
\.


--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('usuario_id_seq', 1, false);


--
-- Name: ciudad ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (id);


--
-- Name: datoscontacto datoscontacto_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY datoscontacto
    ADD CONSTRAINT datoscontacto_pkey PRIMARY KEY (id);


--
-- Name: etiqueta_multimediadron etiqueta_multimediadron_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY etiqueta_multimediadron
    ADD CONSTRAINT etiqueta_multimediadron_pkey PRIMARY KEY (etiquetaid, multimediadronid);


--
-- Name: etiqueta etiqueta_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY etiqueta
    ADD CONSTRAINT etiqueta_pkey PRIMARY KEY (id);


--
-- Name: mensaje mensaje_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensaje
    ADD CONSTRAINT mensaje_pkey PRIMARY KEY (id);


--
-- Name: mensajemultimedia_etiqueta mensajemultimedia_etiqueta_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia_etiqueta
    ADD CONSTRAINT mensajemultimedia_etiqueta_pkey PRIMARY KEY (mensajemultimediaid, etiquetaid);


--
-- Name: mensajemultimedia mensajemultimedia_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia
    ADD CONSTRAINT mensajemultimedia_pkey PRIMARY KEY (id);


--
-- Name: multimediadron multimediadron_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY multimediadron
    ADD CONSTRAINT multimediadron_pkey PRIMARY KEY (id);


--
-- Name: streamingdron streamingdron_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY streamingdron
    ADD CONSTRAINT streamingdron_pkey PRIMARY KEY (id);


--
-- Name: tipomultimedia tipomultimedia_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY tipomultimedia
    ADD CONSTRAINT tipomultimedia_pkey PRIMARY KEY (id);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: etiqueta_multimediadron fketiqueta_m327843; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY etiqueta_multimediadron
    ADD CONSTRAINT fketiqueta_m327843 FOREIGN KEY (multimediadronid) REFERENCES multimediadron(id);


--
-- Name: etiqueta_multimediadron fketiqueta_m328276; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY etiqueta_multimediadron
    ADD CONSTRAINT fketiqueta_m328276 FOREIGN KEY (etiquetaid) REFERENCES etiqueta(id);


--
-- Name: mensaje fkmensaje143157; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensaje
    ADD CONSTRAINT fkmensaje143157 FOREIGN KEY (tipomultimediaid) REFERENCES tipomultimedia(id);


--
-- Name: mensaje fkmensaje800505; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensaje
    ADD CONSTRAINT fkmensaje800505 FOREIGN KEY (datoscontactoid) REFERENCES datoscontacto(id);


--
-- Name: mensajemultimedia fkmensajemul244658; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia
    ADD CONSTRAINT fkmensajemul244658 FOREIGN KEY (mensajeid) REFERENCES mensaje(id);


--
-- Name: mensajemultimedia_etiqueta fkmensajemul295377; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia_etiqueta
    ADD CONSTRAINT fkmensajemul295377 FOREIGN KEY (mensajemultimediaid) REFERENCES mensajemultimedia(id);


--
-- Name: mensajemultimedia_etiqueta fkmensajemul746345; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY mensajemultimedia_etiqueta
    ADD CONSTRAINT fkmensajemul746345 FOREIGN KEY (etiquetaid) REFERENCES etiqueta(id);


--
-- Name: multimediadron fkmultimedia115904; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY multimediadron
    ADD CONSTRAINT fkmultimedia115904 FOREIGN KEY (ciudadid) REFERENCES ciudad(id);


--
-- Name: multimediadron fkmultimedia824207; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY multimediadron
    ADD CONSTRAINT fkmultimedia824207 FOREIGN KEY (tipomultimediaid) REFERENCES tipomultimedia(id);


--
-- Name: streamingdron fkstreamingd628299; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY streamingdron
    ADD CONSTRAINT fkstreamingd628299 FOREIGN KEY (tipomultimediaid) REFERENCES tipomultimedia(id);


--
-- PostgreSQL database dump complete
--

