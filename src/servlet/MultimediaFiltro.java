/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import capaServicio.Servicio;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author kons
 */
@WebServlet(name = "MultimediaFiltro", urlPatterns = {"/MultimediaFiltro"})
public class MultimediaFiltro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MultimediaFiltro</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MultimediaFiltro at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String buscador = request.getParameter("buscador");
            String tmultimedia = request.getParameter("tmultimedia");
            String ciudad = request.getParameter("ciudad");
            Servicio s = new Servicio();
            System.out.println("ENTRO");
        try {
            String multimedia = s.obtenerListaMultimediaFiltro(Integer.parseInt(tmultimedia), Integer.parseInt(ciudad), buscador);
            System.out.println(multimedia);
            request.setAttribute("multimedia", multimedia);
        } catch (Exception ex) { 
            Logger.getLogger(MultimediaFiltro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        	if(tmultimedia.equals("1")){
        		request.getRequestDispatcher("/dashboard/audios.jsp?tipo="+tmultimedia).forward(request, response);
        	}
        	if(tmultimedia.equals("2")){
        		request.getRequestDispatcher("/dashboard/imagenes.jsp?tipo="+tmultimedia).forward(request, response);
        	}
        	if(tmultimedia.equals("3")){
        		System.out.println("ent3");
        		request.getRequestDispatcher("/dashboard/videos.jsp?tipo="+tmultimedia).forward(request, response);
        	}
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
