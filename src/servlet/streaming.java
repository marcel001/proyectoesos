package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import capaNegocio.streamingDronNegocio;

/**
 * Servlet implementation class streaming
 */
@WebServlet("/streaming")
public class streaming extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public streaming() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Crea un objeto para poder utilizar el metodo
		streamingDronNegocio multimedia = new streamingDronNegocio();
		// Crea un arreglo de tipo MultimediaDron que contiene los registros de
		// la base de datos
		streamingDronNegocio[] multimediaList = multimedia.listData();
		// Pasa al jsp el arreglo con el nombre resultado
		request.setAttribute("resultado", multimediaList);
		// redirije al jsp con el atributo resultado
		request.getRequestDispatcher("/dashboard/historial.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
