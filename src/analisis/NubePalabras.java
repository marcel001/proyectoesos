package analisis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class NubePalabras
 */
@WebServlet("/NubePalabras")
public class NubePalabras extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NubePalabras() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try{
	        Class.forName("org.postgresql.Driver").newInstance();
	        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/pds", "root", "root") ;
	        Statement stmt = conn.createStatement() ;
	        String query = "select comment from comments;" ;
	        ResultSet rs = stmt.executeQuery(query);
	        
	        WordCloud wc = new WordCloud(rs,22);
	        wc.count();
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().append(wc.getJSON());
	        
	        conn.close();
	        rs.close();
        
        } catch(Exception e){
        	e.printStackTrace();

        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
