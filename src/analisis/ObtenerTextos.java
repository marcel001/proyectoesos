package analisis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ObtenerTextos
 */
@WebServlet("/ObtenerTextos")
public class ObtenerTextos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ObtenerTextos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
		Class.forName("org.postgresql.Driver").newInstance();
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/pds", "root", "root") ;
        Statement stmt = conn.createStatement() ;
        String query = "select id, comment from comments WHERE comment like '%" + request.getParameter("tag").replaceAll("#", "") + "%';" ;
        System.out.println(request.getParameter("tag"));
        ResultSet rs = stmt.executeQuery(query);
        while(rs.next()){
    		response.getWriter().append(rs.getString(2) + "\n\n");
        }
        rs.close();
        stmt.close();
        conn.close();
        }
        catch(Exception e){
        	e.printStackTrace();
        	
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
