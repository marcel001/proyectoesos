package capaNegocios;

import java.sql.Timestamp;
import java.util.Set;

import org.orm.PersistentException;

import orm.*;

public class MultimediaDronNegocio {


	private int id;
	
	private Double altitud;
	
	private String longitud;
	
	private String latitud;
	
	private String url;
	
	private String urlReducida;
	
	private java.sql.Timestamp fecha;
	
	private Integer numViews;
	
	private orm.TipoMultimedia tipomultimedia;
	
	private java.util.Set ORM_etiqueta = new java.util.HashSet();

	public MultimediaDronNegocio() {
		super();
	}

	public MultimediaDronNegocio(int id, Double altitud, String longitud, String latitud, String url,
			String urlReducida, Timestamp fecha, Integer numViews, TipoMultimedia tipomultimedia, Set oRM_etiqueta) {
		super();
		this.id = id;
		this.altitud = altitud;
		this.longitud = longitud;
		this.latitud = latitud;
		this.url = url;
		this.urlReducida = urlReducida;
		this.fecha = fecha;
		this.numViews = numViews;
		this.tipomultimedia = tipomultimedia;
		ORM_etiqueta = oRM_etiqueta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getAltitud() {
		return altitud;
	}

	public void setAltitud(Double altitud) {
		this.altitud = altitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlReducida() {
		return urlReducida;
	}

	public void setUrlReducida(String urlReducida) {
		this.urlReducida = urlReducida;
	}

	public java.sql.Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(java.sql.Timestamp fecha) {
		this.fecha = fecha;
	}

	public Integer getNumViews() {
		return numViews;
	}

	public void setNumViews(Integer numViews) {
		this.numViews = numViews;
	}

	public orm.TipoMultimedia getTipomultimedia() {
		return tipomultimedia;
	}

	public void setTipomultimedia(orm.TipoMultimedia tipomultimedia) {
		this.tipomultimedia = tipomultimedia;
	}

	public java.util.Set getORM_etiqueta() {
		return ORM_etiqueta;
	}

	public void setORM_etiqueta(java.util.Set oRM_etiqueta) {
		ORM_etiqueta = oRM_etiqueta;
	}
	
	public MultimediaDronNegocio[] listData() {
		orm.MultimediaDron[] aux;
		MultimediaDronNegocio[] out = null;
		try {
			aux = orm.MultimediaDronDAO.listMultimediaDronByQuery(null, null);
			out = fromORMtoMultimediaDron(aux);
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return out;
	}

	public MultimediaDronNegocio[] fromORMtoMultimediaDron(orm.MultimediaDron[] multimedia) {
		MultimediaDronNegocio[] multimediaDron = new MultimediaDronNegocio[multimedia.length];
		for (int i = 0; i < multimediaDron.length; i++) {
			MultimediaDronNegocio sd = new MultimediaDronNegocio();
			sd.setId((int)(multimedia[i].getId()));
			sd.setAltitud((Double)(multimedia[i].getAltitud()));
			sd.setLongitud(multimedia[i].getLongitud());
			sd.setLatitud(multimedia[i].getLongitud());
			sd.setUrl(multimedia[i].getUrl());
			sd.setUrlReducida(multimedia[i].getUrlReducida());
			sd.setFecha(multimedia[i].getFecha());
			multimediaDron[i] = sd;
		}
		return multimediaDron;
	}
	
	
}
