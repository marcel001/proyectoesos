package capaNegocios;

public class TipoMultimediaNegocio {

	
	private int id;
	
	private String tipo;
	
	private java.util.Set ORM_multimediadron = new java.util.HashSet();
	
	private java.util.Set ORM_mensaje = new java.util.HashSet();
	
	private java.util.Set ORM_mensajemultimedida = new java.util.HashSet();

	public TipoMultimediaNegocio() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public java.util.Set getORM_multimediadron() {
		return ORM_multimediadron;
	}

	public void setORM_multimediadron(java.util.Set oRM_multimediadron) {
		ORM_multimediadron = oRM_multimediadron;
	}

	public java.util.Set getORM_mensaje() {
		return ORM_mensaje;
	}

	public void setORM_mensaje(java.util.Set oRM_mensaje) {
		ORM_mensaje = oRM_mensaje;
	}

	public java.util.Set getORM_mensajemultimedida() {
		return ORM_mensajemultimedida;
	}

	public void setORM_mensajemultimedida(java.util.Set oRM_mensajemultimedida) {
		ORM_mensajemultimedida = oRM_mensajemultimedida;
	}
	
	
}
