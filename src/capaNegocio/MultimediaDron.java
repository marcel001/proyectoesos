package capaNegocio;

import java.sql.Timestamp;
import java.util.*;
import org.hibernate.criterion.Restrictions;
import org.orm.PersistentException;

public class MultimediaDron {

	TipoMultimedia tipoMultimedia;
	Ciudad ciudad;
	private int id;
	private Double altitud;
	private String longitud;
	private String latitud;
	private String url;
	private String urlReducida;
	private Timestamp fecha;

    public TipoMultimedia getTipoMultimedia() {
        return tipoMultimedia;
    }

    public void setTipoMultimedia(TipoMultimedia tipoMultimedia) {
        this.tipoMultimedia = tipoMultimedia;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getAltitud() {
        return altitud;
    }

    public void setAltitud(Double altitud) {
        this.altitud = altitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlReducida() {
        return urlReducida;
    }

    public void setUrlReducida(String urlReducida) {
        this.urlReducida = urlReducida;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public MultimediaDron[] obtenerListaMultimedia()throws PersistentException{
        
        MultimediaDron rlist[]= null;
        try{
            orm.MultimediaDron lormMultimediaDron[]= orm.MultimediaDronDAO.listMultimediaDronByQuery(null, null);
            rlist = new MultimediaDron[lormMultimediaDron.length];

            
            
            
            for(int i=0;i<lormMultimediaDron.length;i++){
                MultimediaDron multimediaDron = new MultimediaDron();
                multimediaDron.setId(lormMultimediaDron[i].getId());
                multimediaDron.setUrl(lormMultimediaDron[i].getUrl());
                rlist[i] = multimediaDron;
            }
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return rlist;
    }
    
     public MultimediaDron[] obtenerListaMultimediaFiltro(int idTipo,int idCiudad,String url)throws PersistentException{
        
        MultimediaDron rlist[]= null;
        try{
            orm.MultimediaDronCriteria cr = new orm.MultimediaDronCriteria();
            if(idTipo!=0){
               cr.add(Restrictions.eq("tipoMultimedia.id", idTipo));

            }
            if(idCiudad!=0){
               cr.add(Restrictions.eq("ciudad.id", idCiudad));

            }
            if(!url.equals("")){
                cr.add(Restrictions.like("url","%"+url+"%"));
            }
            orm.MultimediaDron lormMultimediaDron[]= orm.MultimediaDronDAO.listMultimediaDronByCriteria(cr);
            rlist = new MultimediaDron[lormMultimediaDron.length];

            
            
            
            for(int i=0;i<lormMultimediaDron.length;i++){
                MultimediaDron multimediaDron = new MultimediaDron();
                multimediaDron.setId(lormMultimediaDron[i].getId());
                multimediaDron.setUrl(lormMultimediaDron[i].getUrl());
                rlist[i] = multimediaDron;
            }
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return rlist;
    }

        
}