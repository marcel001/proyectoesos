package capaNegocio;

import java.sql.Timestamp;

public class StreamingDron {

	TipoMultimedia tipoMultimedia;
	private int id;
	private String url;
	private Timestamp fecha;

    public TipoMultimedia getTipoMultimedia() {
        return tipoMultimedia;
    }

    public void setTipoMultimedia(TipoMultimedia tipoMultimedia) {
        this.tipoMultimedia = tipoMultimedia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

        
        
}