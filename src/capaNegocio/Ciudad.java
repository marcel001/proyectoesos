package capaNegocio;

import java.util.*;
import org.orm.PersistentException;

public class Ciudad {


	private int id;
	private String nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
        
    public Ciudad[] obtenerListaCiudad()throws PersistentException{
        
        Ciudad rlist[]= null;
        try{
            orm.Ciudad lormCiudad[]= orm.CiudadDAO.listCiudadByQuery(null, null);
            rlist = new Ciudad[lormCiudad.length];

            
            
            
            for(int i=0;i<lormCiudad.length;i++){
                Ciudad ciudad = new Ciudad();
                ciudad.setId(lormCiudad[i].getId());
                ciudad.setNombre(lormCiudad[i].getNombre());
                rlist[i] = ciudad;
            }
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return rlist;
    }
    
}