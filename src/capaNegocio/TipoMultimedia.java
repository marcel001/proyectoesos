package capaNegocio;

import java.util.*;
import org.orm.PersistentException;

public class TipoMultimedia {

	private int id;
	private String tipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
        
    public TipoMultimedia[] obtenerListaTipoMultimedia()throws PersistentException{
        
        TipoMultimedia rlist[]= null;
        try{
            orm.TipoMultimedia lormTipoMultimedia[]= orm.TipoMultimediaDAO.listTipoMultimediaByQuery(null, null);
            rlist = new TipoMultimedia[lormTipoMultimedia.length];

            
            
            
            for(int i=0;i<lormTipoMultimedia.length;i++){
                TipoMultimedia tipoMultimedia = new TipoMultimedia();
                tipoMultimedia.setId(lormTipoMultimedia[i].getId());
                tipoMultimedia.setTipo(lormTipoMultimedia[i].getTipo());
                rlist[i] = tipoMultimedia;
            }
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return rlist;
    }

}