package capaNegocio;

import java.sql.Timestamp;

import org.orm.PersistentException;

import orm.TipoMultimedia;

public class streamingDronNegocio {

private int id;
	
	private String url;
	
	private java.sql.Timestamp fecha;
	
	private orm.TipoMultimedia tipoMultimedia;

	
	
	public streamingDronNegocio() {
		super();
	}



	public streamingDronNegocio(int id, String url, Timestamp fecha, TipoMultimedia tipoMultimedia) {
		super();
		this.id = id;
		this.url = url;
		this.fecha = fecha;
		this.tipoMultimedia = tipoMultimedia;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public java.sql.Timestamp getFecha() {
		return fecha;
	}



	public void setFecha(java.sql.Timestamp fecha) {
		this.fecha = fecha;
	}



	public orm.TipoMultimedia getTipoMultimedia() {
		return tipoMultimedia;
	}



	public void setTipoMultimedia(orm.TipoMultimedia tipoMultimedia) {
		this.tipoMultimedia = tipoMultimedia;
	}
	
	//Metodo que entrega la lista con los registros de la base de datos
		public streamingDronNegocio[] listData() {
			orm.StreamingDron[] aux;
			streamingDronNegocio[] out = null;
			try {
				aux = orm.StreamingDronDAO.listStreamingDronByQuery(null, null);
				out = fromORMtoMultimediaDron(aux);
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return out;
		}

		//Metodo que obtiene los registros de la base de datos y los pasa a un arreglo del mismo tipo pero de la 
		//capa de negocio
		public streamingDronNegocio[] fromORMtoMultimediaDron(orm.StreamingDron[] multimedia) {
			streamingDronNegocio[] streamingDron = new streamingDronNegocio[multimedia.length];
			for (int i = 0; i < streamingDron.length; i++) {
				streamingDronNegocio sd = new streamingDronNegocio();
				sd.setId((int)(multimedia[i].getId()));
				sd.setUrl((multimedia[i].getUrl()));
				sd.setFecha(multimedia[i].getFecha());
				streamingDron[i] = sd;
			}
			return streamingDron;
		}
		
		private static final int ROW_COUNT = 100;
		public void listTest(){
			int length =0;
			System.out.println("Listing StreamingDron...");
			orm.StreamingDron[] ormStreamingDrons;
			try {
				ormStreamingDrons = orm.StreamingDronDAO.listStreamingDronByQuery(null, null);
				length = Math.min(ormStreamingDrons.length, ROW_COUNT);
				for (int i = 0; i < length; i++) {
					System.out.println(ormStreamingDrons[i]);
				}
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println(length + " record(s) retrieved.");
		}
	
}
