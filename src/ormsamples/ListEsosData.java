/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class ListEsosData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing DatosContacto...");
		orm.DatosContacto[] ormDatosContactos = orm.DatosContactoDAO.listDatosContactoByQuery(null, null);
		int length = Math.min(ormDatosContactos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormDatosContactos[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Mensaje...");
		orm.Mensaje[] ormMensajes = orm.MensajeDAO.listMensajeByQuery(null, null);
		length = Math.min(ormMensajes.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMensajes[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing TipoMultimedia...");
		orm.TipoMultimedia[] ormTipoMultimedias = orm.TipoMultimediaDAO.listTipoMultimediaByQuery(null, null);
		length = Math.min(ormTipoMultimedias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormTipoMultimedias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing MensajeMultimedia...");
		orm.MensajeMultimedia[] ormMensajeMultimedias = orm.MensajeMultimediaDAO.listMensajeMultimediaByQuery(null, null);
		length = Math.min(ormMensajeMultimedias.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMensajeMultimedias[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Etiqueta...");
		orm.Etiqueta[] ormEtiquetas = orm.EtiquetaDAO.listEtiquetaByQuery(null, null);
		length = Math.min(ormEtiquetas.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormEtiquetas[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing MultimediaDron...");
		orm.MultimediaDron[] ormMultimediaDrons = orm.MultimediaDronDAO.listMultimediaDronByQuery(null, null);
		length = Math.min(ormMultimediaDrons.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormMultimediaDrons[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing StreamingDron...");
		orm.StreamingDron[] ormStreamingDrons = orm.StreamingDronDAO.listStreamingDronByQuery(null, null);
		length = Math.min(ormStreamingDrons.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormStreamingDrons[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Ciudad...");
		orm.Ciudad[] ormCiudads = orm.CiudadDAO.listCiudadByQuery(null, null);
		length = Math.min(ormCiudads.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormCiudads[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Usuario...");
		orm.Usuario[] ormUsuarios = orm.UsuarioDAO.listUsuarioByQuery(null, null);
		length = Math.min(ormUsuarios.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(ormUsuarios[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing DatosContacto by Criteria...");
		orm.DatosContactoCriteria lormDatosContactoCriteria = new orm.DatosContactoCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormDatosContactoCriteria.id.eq();
		lormDatosContactoCriteria.setMaxResults(ROW_COUNT);
		orm.DatosContacto[] ormDatosContactos = lormDatosContactoCriteria.listDatosContacto();
		int length =ormDatosContactos== null ? 0 : Math.min(ormDatosContactos.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormDatosContactos[i]);
		}
		System.out.println(length + " DatosContacto record(s) retrieved."); 
		
		System.out.println("Listing Mensaje by Criteria...");
		orm.MensajeCriteria lormMensajeCriteria = new orm.MensajeCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMensajeCriteria.id.eq();
		lormMensajeCriteria.setMaxResults(ROW_COUNT);
		orm.Mensaje[] ormMensajes = lormMensajeCriteria.listMensaje();
		length =ormMensajes== null ? 0 : Math.min(ormMensajes.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMensajes[i]);
		}
		System.out.println(length + " Mensaje record(s) retrieved."); 
		
		System.out.println("Listing TipoMultimedia by Criteria...");
		orm.TipoMultimediaCriteria lormTipoMultimediaCriteria = new orm.TipoMultimediaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormTipoMultimediaCriteria.id.eq();
		lormTipoMultimediaCriteria.setMaxResults(ROW_COUNT);
		orm.TipoMultimedia[] ormTipoMultimedias = lormTipoMultimediaCriteria.listTipoMultimedia();
		length =ormTipoMultimedias== null ? 0 : Math.min(ormTipoMultimedias.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormTipoMultimedias[i]);
		}
		System.out.println(length + " TipoMultimedia record(s) retrieved."); 
		
		System.out.println("Listing MensajeMultimedia by Criteria...");
		orm.MensajeMultimediaCriteria lormMensajeMultimediaCriteria = new orm.MensajeMultimediaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMensajeMultimediaCriteria.id.eq();
		lormMensajeMultimediaCriteria.setMaxResults(ROW_COUNT);
		orm.MensajeMultimedia[] ormMensajeMultimedias = lormMensajeMultimediaCriteria.listMensajeMultimedia();
		length =ormMensajeMultimedias== null ? 0 : Math.min(ormMensajeMultimedias.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMensajeMultimedias[i]);
		}
		System.out.println(length + " MensajeMultimedia record(s) retrieved."); 
		
		System.out.println("Listing Etiqueta by Criteria...");
		orm.EtiquetaCriteria lormEtiquetaCriteria = new orm.EtiquetaCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormEtiquetaCriteria.id.eq();
		lormEtiquetaCriteria.setMaxResults(ROW_COUNT);
		orm.Etiqueta[] ormEtiquetas = lormEtiquetaCriteria.listEtiqueta();
		length =ormEtiquetas== null ? 0 : Math.min(ormEtiquetas.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormEtiquetas[i]);
		}
		System.out.println(length + " Etiqueta record(s) retrieved."); 
		
		System.out.println("Listing MultimediaDron by Criteria...");
		orm.MultimediaDronCriteria lormMultimediaDronCriteria = new orm.MultimediaDronCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormMultimediaDronCriteria.id.eq();
		lormMultimediaDronCriteria.setMaxResults(ROW_COUNT);
		orm.MultimediaDron[] ormMultimediaDrons = lormMultimediaDronCriteria.listMultimediaDron();
		length =ormMultimediaDrons== null ? 0 : Math.min(ormMultimediaDrons.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormMultimediaDrons[i]);
		}
		System.out.println(length + " MultimediaDron record(s) retrieved."); 
		
		System.out.println("Listing StreamingDron by Criteria...");
		orm.StreamingDronCriteria lormStreamingDronCriteria = new orm.StreamingDronCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormStreamingDronCriteria.id.eq();
		lormStreamingDronCriteria.setMaxResults(ROW_COUNT);
		orm.StreamingDron[] ormStreamingDrons = lormStreamingDronCriteria.listStreamingDron();
		length =ormStreamingDrons== null ? 0 : Math.min(ormStreamingDrons.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormStreamingDrons[i]);
		}
		System.out.println(length + " StreamingDron record(s) retrieved."); 
		
		System.out.println("Listing Ciudad by Criteria...");
		orm.CiudadCriteria lormCiudadCriteria = new orm.CiudadCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormCiudadCriteria.id.eq();
		lormCiudadCriteria.setMaxResults(ROW_COUNT);
		orm.Ciudad[] ormCiudads = lormCiudadCriteria.listCiudad();
		length =ormCiudads== null ? 0 : Math.min(ormCiudads.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormCiudads[i]);
		}
		System.out.println(length + " Ciudad record(s) retrieved."); 
		
		System.out.println("Listing Usuario by Criteria...");
		orm.UsuarioCriteria lormUsuarioCriteria = new orm.UsuarioCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lormUsuarioCriteria.id.eq();
		lormUsuarioCriteria.setMaxResults(ROW_COUNT);
		orm.Usuario[] ormUsuarios = lormUsuarioCriteria.listUsuario();
		length =ormUsuarios== null ? 0 : Math.min(ormUsuarios.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(ormUsuarios[i]);
		}
		System.out.println(length + " Usuario record(s) retrieved."); 
		
	}
	
	public static void main(String[] args) {
		try {
			ListEsosData listEsosData = new ListEsosData();
			try {
				listEsosData.listTestData();
				//listEsosData.listByCriteria();
			}
			finally {
				orm.EsosPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
