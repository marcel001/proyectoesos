/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class CreateEsosData {
	public void createTestData() throws PersistentException {
		PersistentTransaction t = orm.EsosPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.DatosContacto lormDatosContacto = orm.DatosContactoDAO.createDatosContacto();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : mensaje, pass, cargo, apellidos, nombres, celular, rut
			orm.DatosContactoDAO.save(lormDatosContacto);
			orm.Mensaje lormMensaje = orm.MensajeDAO.createMensaje();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : mensajeMultimedia, tipoMultimedia, datosContacto, fecha
			orm.MensajeDAO.save(lormMensaje);
			orm.TipoMultimedia lormTipoMultimedia = orm.TipoMultimediaDAO.createTipoMultimedia();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : streamingDron, multimediaDron, mensaje, tipo
			orm.TipoMultimediaDAO.save(lormTipoMultimedia);
			orm.MensajeMultimedia lormMensajeMultimedia = orm.MensajeMultimediaDAO.createMensajeMultimedia();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : etiqueta, mensaje, fecha
			orm.MensajeMultimediaDAO.save(lormMensajeMultimedia);
			orm.Etiqueta lormEtiqueta = orm.EtiquetaDAO.createEtiqueta();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multimediaDron, mensajeMultimedia, nombre
			orm.EtiquetaDAO.save(lormEtiqueta);
			orm.MultimediaDron lormMultimediaDron = orm.MultimediaDronDAO.createMultimediaDron();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : etiqueta, ciudad, tipoMultimedia, fecha, url
			orm.MultimediaDronDAO.save(lormMultimediaDron);
			orm.StreamingDron lormStreamingDron = orm.StreamingDronDAO.createStreamingDron();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : tipoMultimedia, fecha, url
			orm.StreamingDronDAO.save(lormStreamingDron);
			orm.Ciudad lormCiudad = orm.CiudadDAO.createCiudad();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : multimediaDron, nombre
			orm.CiudadDAO.save(lormCiudad);
			orm.Usuario lormUsuario = orm.UsuarioDAO.createUsuario();
			// TODO Initialize the properties of the persistent object here, the following properties must be initialized before saving : pass, nick
			orm.UsuarioDAO.save(lormUsuario);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CreateEsosData createEsosData = new CreateEsosData();
			try {
				createEsosData.createTestData();
			}
			finally {
				orm.EsosPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
