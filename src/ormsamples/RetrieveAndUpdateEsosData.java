/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateEsosData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = orm.EsosPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.DatosContacto lormDatosContacto = orm.DatosContactoDAO.loadDatosContactoByQuery(null, null);
			// Update the properties of the persistent object
			orm.DatosContactoDAO.save(lormDatosContacto);
			orm.Mensaje lormMensaje = orm.MensajeDAO.loadMensajeByQuery(null, null);
			// Update the properties of the persistent object
			orm.MensajeDAO.save(lormMensaje);
			orm.TipoMultimedia lormTipoMultimedia = orm.TipoMultimediaDAO.loadTipoMultimediaByQuery(null, null);
			// Update the properties of the persistent object
			orm.TipoMultimediaDAO.save(lormTipoMultimedia);
			orm.MensajeMultimedia lormMensajeMultimedia = orm.MensajeMultimediaDAO.loadMensajeMultimediaByQuery(null, null);
			// Update the properties of the persistent object
			orm.MensajeMultimediaDAO.save(lormMensajeMultimedia);
			orm.Etiqueta lormEtiqueta = orm.EtiquetaDAO.loadEtiquetaByQuery(null, null);
			// Update the properties of the persistent object
			orm.EtiquetaDAO.save(lormEtiqueta);
			orm.MultimediaDron lormMultimediaDron = orm.MultimediaDronDAO.loadMultimediaDronByQuery(null, null);
			// Update the properties of the persistent object
			orm.MultimediaDronDAO.save(lormMultimediaDron);
			orm.StreamingDron lormStreamingDron = orm.StreamingDronDAO.loadStreamingDronByQuery(null, null);
			// Update the properties of the persistent object
			orm.StreamingDronDAO.save(lormStreamingDron);
			orm.Ciudad lormCiudad = orm.CiudadDAO.loadCiudadByQuery(null, null);
			// Update the properties of the persistent object
			orm.CiudadDAO.save(lormCiudad);
			orm.Usuario lormUsuario = orm.UsuarioDAO.loadUsuarioByQuery(null, null);
			// Update the properties of the persistent object
			orm.UsuarioDAO.save(lormUsuario);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving DatosContacto by DatosContactoCriteria");
		orm.DatosContactoCriteria lormDatosContactoCriteria = new orm.DatosContactoCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormDatosContactoCriteria.id.eq();
		System.out.println(lormDatosContactoCriteria.uniqueDatosContacto());
		
		System.out.println("Retrieving Mensaje by MensajeCriteria");
		orm.MensajeCriteria lormMensajeCriteria = new orm.MensajeCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMensajeCriteria.id.eq();
		System.out.println(lormMensajeCriteria.uniqueMensaje());
		
		System.out.println("Retrieving TipoMultimedia by TipoMultimediaCriteria");
		orm.TipoMultimediaCriteria lormTipoMultimediaCriteria = new orm.TipoMultimediaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormTipoMultimediaCriteria.id.eq();
		System.out.println(lormTipoMultimediaCriteria.uniqueTipoMultimedia());
		
		System.out.println("Retrieving MensajeMultimedia by MensajeMultimediaCriteria");
		orm.MensajeMultimediaCriteria lormMensajeMultimediaCriteria = new orm.MensajeMultimediaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMensajeMultimediaCriteria.id.eq();
		System.out.println(lormMensajeMultimediaCriteria.uniqueMensajeMultimedia());
		
		System.out.println("Retrieving Etiqueta by EtiquetaCriteria");
		orm.EtiquetaCriteria lormEtiquetaCriteria = new orm.EtiquetaCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormEtiquetaCriteria.id.eq();
		System.out.println(lormEtiquetaCriteria.uniqueEtiqueta());
		
		System.out.println("Retrieving MultimediaDron by MultimediaDronCriteria");
		orm.MultimediaDronCriteria lormMultimediaDronCriteria = new orm.MultimediaDronCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormMultimediaDronCriteria.id.eq();
		System.out.println(lormMultimediaDronCriteria.uniqueMultimediaDron());
		
		System.out.println("Retrieving StreamingDron by StreamingDronCriteria");
		orm.StreamingDronCriteria lormStreamingDronCriteria = new orm.StreamingDronCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormStreamingDronCriteria.id.eq();
		System.out.println(lormStreamingDronCriteria.uniqueStreamingDron());
		
		System.out.println("Retrieving Ciudad by CiudadCriteria");
		orm.CiudadCriteria lormCiudadCriteria = new orm.CiudadCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormCiudadCriteria.id.eq();
		System.out.println(lormCiudadCriteria.uniqueCiudad());
		
		System.out.println("Retrieving Usuario by UsuarioCriteria");
		orm.UsuarioCriteria lormUsuarioCriteria = new orm.UsuarioCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lormUsuarioCriteria.id.eq();
		System.out.println(lormUsuarioCriteria.uniqueUsuario());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateEsosData retrieveAndUpdateEsosData = new RetrieveAndUpdateEsosData();
			try {
				retrieveAndUpdateEsosData.retrieveAndUpdateTestData();
				//retrieveAndUpdateEsosData.retrieveByCriteria();
			}
			finally {
				orm.EsosPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
