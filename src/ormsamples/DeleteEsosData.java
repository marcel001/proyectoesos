/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package ormsamples;

import org.orm.*;
public class DeleteEsosData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = orm.EsosPersistentManager.instance().getSession().beginTransaction();
		try {
			orm.DatosContacto lormDatosContacto = orm.DatosContactoDAO.loadDatosContactoByQuery(null, null);
			// Delete the persistent object
			orm.DatosContactoDAO.delete(lormDatosContacto);
			orm.Mensaje lormMensaje = orm.MensajeDAO.loadMensajeByQuery(null, null);
			// Delete the persistent object
			orm.MensajeDAO.delete(lormMensaje);
			orm.TipoMultimedia lormTipoMultimedia = orm.TipoMultimediaDAO.loadTipoMultimediaByQuery(null, null);
			// Delete the persistent object
			orm.TipoMultimediaDAO.delete(lormTipoMultimedia);
			orm.MensajeMultimedia lormMensajeMultimedia = orm.MensajeMultimediaDAO.loadMensajeMultimediaByQuery(null, null);
			// Delete the persistent object
			orm.MensajeMultimediaDAO.delete(lormMensajeMultimedia);
			orm.Etiqueta lormEtiqueta = orm.EtiquetaDAO.loadEtiquetaByQuery(null, null);
			// Delete the persistent object
			orm.EtiquetaDAO.delete(lormEtiqueta);
			orm.MultimediaDron lormMultimediaDron = orm.MultimediaDronDAO.loadMultimediaDronByQuery(null, null);
			// Delete the persistent object
			orm.MultimediaDronDAO.delete(lormMultimediaDron);
			orm.StreamingDron lormStreamingDron = orm.StreamingDronDAO.loadStreamingDronByQuery(null, null);
			// Delete the persistent object
			orm.StreamingDronDAO.delete(lormStreamingDron);
			orm.Ciudad lormCiudad = orm.CiudadDAO.loadCiudadByQuery(null, null);
			// Delete the persistent object
			orm.CiudadDAO.delete(lormCiudad);
			orm.Usuario lormUsuario = orm.UsuarioDAO.loadUsuarioByQuery(null, null);
			// Delete the persistent object
			orm.UsuarioDAO.delete(lormUsuario);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteEsosData deleteEsosData = new DeleteEsosData();
			try {
				deleteEsosData.deleteTestData();
			}
			finally {
				orm.EsosPersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
