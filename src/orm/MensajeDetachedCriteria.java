/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MensajeDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final TimestampExpression fecha;
	public final StringExpression latitud;
	public final StringExpression longitud;
	public final StringExpression mensaje;
	public final IntegerExpression datosContactoId;
	public final AssociationExpression datosContacto;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	public final CollectionExpression mensajeMultimedia;
	
	public MensajeDetachedCriteria() {
		super(orm.Mensaje.class, orm.MensajeCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		mensaje = new StringExpression("mensaje", this.getDetachedCriteria());
		datosContactoId = new IntegerExpression("datosContacto.id", this.getDetachedCriteria());
		datosContacto = new AssociationExpression("datosContacto", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this.getDetachedCriteria());
	}
	
	public MensajeDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.MensajeCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		mensaje = new StringExpression("mensaje", this.getDetachedCriteria());
		datosContactoId = new IntegerExpression("datosContacto.id", this.getDetachedCriteria());
		datosContacto = new AssociationExpression("datosContacto", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this.getDetachedCriteria());
	}
	
	public DatosContactoDetachedCriteria createDatosContactoCriteria() {
		return new DatosContactoDetachedCriteria(createCriteria("datosContacto"));
	}
	
	public TipoMultimediaDetachedCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaDetachedCriteria(createCriteria("tipoMultimedia"));
	}
	
	public MensajeMultimediaDetachedCriteria createMensajeMultimediaCriteria() {
		return new MensajeMultimediaDetachedCriteria(createCriteria("ORM_MensajeMultimedia"));
	}
	
	public Mensaje uniqueMensaje(PersistentSession session) {
		return (Mensaje) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Mensaje[] listMensaje(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Mensaje[]) list.toArray(new Mensaje[list.size()]);
	}
}

