/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class MensajeMultimediaDAO {
	public static MensajeMultimedia loadMensajeMultimediaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMensajeMultimediaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia getMensajeMultimediaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getMensajeMultimediaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMensajeMultimediaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia getMensajeMultimediaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getMensajeMultimediaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (MensajeMultimedia) session.load(orm.MensajeMultimedia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia getMensajeMultimediaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (MensajeMultimedia) session.get(orm.MensajeMultimedia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (MensajeMultimedia) session.load(orm.MensajeMultimedia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia getMensajeMultimediaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (MensajeMultimedia) session.get(orm.MensajeMultimedia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMensajeMultimedia(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryMensajeMultimedia(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMensajeMultimedia(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryMensajeMultimedia(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia[] listMensajeMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listMensajeMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia[] listMensajeMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listMensajeMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMensajeMultimedia(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MensajeMultimedia as MensajeMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMensajeMultimedia(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MensajeMultimedia as MensajeMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("MensajeMultimedia", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia[] listMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryMensajeMultimedia(session, condition, orderBy);
			return (MensajeMultimedia[]) list.toArray(new MensajeMultimedia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia[] listMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryMensajeMultimedia(session, condition, orderBy, lockMode);
			return (MensajeMultimedia[]) list.toArray(new MensajeMultimedia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMensajeMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMensajeMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		MensajeMultimedia[] mensajeMultimedias = listMensajeMultimediaByQuery(session, condition, orderBy);
		if (mensajeMultimedias != null && mensajeMultimedias.length > 0)
			return mensajeMultimedias[0];
		else
			return null;
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		MensajeMultimedia[] mensajeMultimedias = listMensajeMultimediaByQuery(session, condition, orderBy, lockMode);
		if (mensajeMultimedias != null && mensajeMultimedias.length > 0)
			return mensajeMultimedias[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateMensajeMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateMensajeMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMensajeMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateMensajeMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MensajeMultimedia as MensajeMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMensajeMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MensajeMultimedia as MensajeMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("MensajeMultimedia", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia createMensajeMultimedia() {
		return new orm.MensajeMultimedia();
	}
	
	public static boolean save(orm.MensajeMultimedia mensajeMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().saveObject(mensajeMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.MensajeMultimedia mensajeMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().deleteObject(mensajeMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.MensajeMultimedia mensajeMultimedia)throws PersistentException {
		try {
			if (mensajeMultimedia.getMensaje() != null) {
				mensajeMultimedia.getMensaje().mensajeMultimedia.remove(mensajeMultimedia);
			}
			
			orm.Etiqueta[] lEtiquetas = mensajeMultimedia.etiqueta.toArray();
			for(int i = 0; i < lEtiquetas.length; i++) {
				lEtiquetas[i].mensajeMultimedia.remove(mensajeMultimedia);
			}
			return delete(mensajeMultimedia);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.MensajeMultimedia mensajeMultimedia, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (mensajeMultimedia.getMensaje() != null) {
				mensajeMultimedia.getMensaje().mensajeMultimedia.remove(mensajeMultimedia);
			}
			
			orm.Etiqueta[] lEtiquetas = mensajeMultimedia.etiqueta.toArray();
			for(int i = 0; i < lEtiquetas.length; i++) {
				lEtiquetas[i].mensajeMultimedia.remove(mensajeMultimedia);
			}
			try {
				session.delete(mensajeMultimedia);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.MensajeMultimedia mensajeMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().refresh(mensajeMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.MensajeMultimedia mensajeMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().evict(mensajeMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MensajeMultimedia loadMensajeMultimediaByCriteria(MensajeMultimediaCriteria mensajeMultimediaCriteria) {
		MensajeMultimedia[] mensajeMultimedias = listMensajeMultimediaByCriteria(mensajeMultimediaCriteria);
		if(mensajeMultimedias == null || mensajeMultimedias.length == 0) {
			return null;
		}
		return mensajeMultimedias[0];
	}
	
	public static MensajeMultimedia[] listMensajeMultimediaByCriteria(MensajeMultimediaCriteria mensajeMultimediaCriteria) {
		return mensajeMultimediaCriteria.listMensajeMultimedia();
	}
}
