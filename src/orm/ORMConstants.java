/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public interface ORMConstants extends org.orm.util.ORMBaseConstants {
	final int KEY_CIUDAD_MULTIMEDIADRON = -1332157825;
	
	final int KEY_DATOSCONTACTO_MENSAJE = 614176938;
	
	final int KEY_ETIQUETA_MENSAJEMULTIMEDIA = 1136679615;
	
	final int KEY_ETIQUETA_MULTIMEDIADRON = -590698557;
	
	final int KEY_MENSAJEMULTIMEDIA_ETIQUETA = 1278735817;
	
	final int KEY_MENSAJEMULTIMEDIA_MENSAJE = -1180698102;
	
	final int KEY_MENSAJE_DATOSCONTACTO = 184064938;
	
	final int KEY_MENSAJE_MENSAJEMULTIMEDIA = -1122104054;
	
	final int KEY_MENSAJE_TIPOMULTIMEDIA = 1835420191;
	
	final int KEY_MULTIMEDIADRON_CIUDAD = -1673762177;
	
	final int KEY_MULTIMEDIADRON_ETIQUETA = -2135367685;
	
	final int KEY_MULTIMEDIADRON_TIPOMULTIMEDIA = 179825606;
	
	final int KEY_STREAMINGDRON_TIPOMULTIMEDIA = 652243087;
	
	final int KEY_TIPOMULTIMEDIA_MENSAJE = 976520031;
	
	final int KEY_TIPOMULTIMEDIA_MULTIMEDIADRON = -1724910760;
	
	final int KEY_TIPOMULTIMEDIA_STREAMINGDRON = 62739183;
	
}
