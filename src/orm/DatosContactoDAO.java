/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class DatosContactoDAO {
	public static DatosContacto loadDatosContactoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadDatosContactoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto getDatosContactoByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getDatosContactoByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadDatosContactoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto getDatosContactoByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getDatosContactoByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (DatosContacto) session.load(orm.DatosContacto.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto getDatosContactoByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (DatosContacto) session.get(orm.DatosContacto.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (DatosContacto) session.load(orm.DatosContacto.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto getDatosContactoByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (DatosContacto) session.get(orm.DatosContacto.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDatosContacto(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryDatosContacto(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDatosContacto(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryDatosContacto(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto[] listDatosContactoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listDatosContactoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto[] listDatosContactoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listDatosContactoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDatosContacto(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.DatosContacto as DatosContacto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryDatosContacto(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.DatosContacto as DatosContacto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("DatosContacto", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto[] listDatosContactoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryDatosContacto(session, condition, orderBy);
			return (DatosContacto[]) list.toArray(new DatosContacto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto[] listDatosContactoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryDatosContacto(session, condition, orderBy, lockMode);
			return (DatosContacto[]) list.toArray(new DatosContacto[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadDatosContactoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadDatosContactoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		DatosContacto[] datosContactos = listDatosContactoByQuery(session, condition, orderBy);
		if (datosContactos != null && datosContactos.length > 0)
			return datosContactos[0];
		else
			return null;
	}
	
	public static DatosContacto loadDatosContactoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		DatosContacto[] datosContactos = listDatosContactoByQuery(session, condition, orderBy, lockMode);
		if (datosContactos != null && datosContactos.length > 0)
			return datosContactos[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateDatosContactoByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateDatosContactoByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDatosContactoByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateDatosContactoByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDatosContactoByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.DatosContacto as DatosContacto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateDatosContactoByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.DatosContacto as DatosContacto");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("DatosContacto", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto createDatosContacto() {
		return new orm.DatosContacto();
	}
	
	public static boolean save(orm.DatosContacto datosContacto) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().saveObject(datosContacto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.DatosContacto datosContacto) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().deleteObject(datosContacto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.DatosContacto datosContacto)throws PersistentException {
		try {
			orm.Mensaje[] lMensajes = datosContacto.mensaje.toArray();
			for(int i = 0; i < lMensajes.length; i++) {
				lMensajes[i].setDatosContacto(null);
			}
			return delete(datosContacto);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.DatosContacto datosContacto, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Mensaje[] lMensajes = datosContacto.mensaje.toArray();
			for(int i = 0; i < lMensajes.length; i++) {
				lMensajes[i].setDatosContacto(null);
			}
			try {
				session.delete(datosContacto);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.DatosContacto datosContacto) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().refresh(datosContacto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.DatosContacto datosContacto) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().evict(datosContacto);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static DatosContacto loadDatosContactoByCriteria(DatosContactoCriteria datosContactoCriteria) {
		DatosContacto[] datosContactos = listDatosContactoByCriteria(datosContactoCriteria);
		if(datosContactos == null || datosContactos.length == 0) {
			return null;
		}
		return datosContactos[0];
	}
	
	public static DatosContacto[] listDatosContactoByCriteria(DatosContactoCriteria datosContactoCriteria) {
		return datosContactoCriteria.listDatosContacto();
	}
}
