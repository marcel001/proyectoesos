/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MultimediaDronDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final DoubleExpression altitud;
	public final StringExpression longitud;
	public final StringExpression latitud;
	public final StringExpression url;
	public final StringExpression urlReducida;
	public final TimestampExpression fecha;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	public final IntegerExpression ciudadId;
	public final AssociationExpression ciudad;
	public final CollectionExpression etiqueta;
	
	public MultimediaDronDetachedCriteria() {
		super(orm.MultimediaDron.class, orm.MultimediaDronCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		altitud = new DoubleExpression("altitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		urlReducida = new StringExpression("urlReducida", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
		ciudadId = new IntegerExpression("ciudad.id", this.getDetachedCriteria());
		ciudad = new AssociationExpression("ciudad", this.getDetachedCriteria());
		etiqueta = new CollectionExpression("ORM_Etiqueta", this.getDetachedCriteria());
	}
	
	public MultimediaDronDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.MultimediaDronCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		altitud = new DoubleExpression("altitud", this.getDetachedCriteria());
		longitud = new StringExpression("longitud", this.getDetachedCriteria());
		latitud = new StringExpression("latitud", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		urlReducida = new StringExpression("urlReducida", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
		ciudadId = new IntegerExpression("ciudad.id", this.getDetachedCriteria());
		ciudad = new AssociationExpression("ciudad", this.getDetachedCriteria());
		etiqueta = new CollectionExpression("ORM_Etiqueta", this.getDetachedCriteria());
	}
	
	public TipoMultimediaDetachedCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaDetachedCriteria(createCriteria("tipoMultimedia"));
	}
	
	public CiudadDetachedCriteria createCiudadCriteria() {
		return new CiudadDetachedCriteria(createCriteria("ciudad"));
	}
	
	public EtiquetaDetachedCriteria createEtiquetaCriteria() {
		return new EtiquetaDetachedCriteria(createCriteria("ORM_Etiqueta"));
	}
	
	public MultimediaDron uniqueMultimediaDron(PersistentSession session) {
		return (MultimediaDron) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public MultimediaDron[] listMultimediaDron(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (MultimediaDron[]) list.toArray(new MultimediaDron[list.size()]);
	}
}

