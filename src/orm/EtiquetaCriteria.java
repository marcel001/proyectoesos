/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EtiquetaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre;
	public final CollectionExpression mensajeMultimedia;
	public final CollectionExpression multimediaDron;
	
	public EtiquetaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		nombre = new StringExpression("nombre", this);
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this);
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this);
	}
	
	public EtiquetaCriteria(PersistentSession session) {
		this(session.createCriteria(Etiqueta.class));
	}
	
	public EtiquetaCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public MensajeMultimediaCriteria createMensajeMultimediaCriteria() {
		return new MensajeMultimediaCriteria(createCriteria("ORM_MensajeMultimedia"));
	}
	
	public MultimediaDronCriteria createMultimediaDronCriteria() {
		return new MultimediaDronCriteria(createCriteria("ORM_MultimediaDron"));
	}
	
	public Etiqueta uniqueEtiqueta() {
		return (Etiqueta) super.uniqueResult();
	}
	
	public Etiqueta[] listEtiqueta() {
		java.util.List list = super.list();
		return (Etiqueta[]) list.toArray(new Etiqueta[list.size()]);
	}
}

