/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MensajeMultimediaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression url;
	public final TimestampExpression fecha;
	public final IntegerExpression mensajeId;
	public final AssociationExpression mensaje;
	public final CollectionExpression etiqueta;
	
	public MensajeMultimediaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		url = new StringExpression("url", this);
		fecha = new TimestampExpression("fecha", this);
		mensajeId = new IntegerExpression("mensaje.id", this);
		mensaje = new AssociationExpression("mensaje", this);
		etiqueta = new CollectionExpression("ORM_Etiqueta", this);
	}
	
	public MensajeMultimediaCriteria(PersistentSession session) {
		this(session.createCriteria(MensajeMultimedia.class));
	}
	
	public MensajeMultimediaCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public MensajeCriteria createMensajeCriteria() {
		return new MensajeCriteria(createCriteria("mensaje"));
	}
	
	public EtiquetaCriteria createEtiquetaCriteria() {
		return new EtiquetaCriteria(createCriteria("ORM_Etiqueta"));
	}
	
	public MensajeMultimedia uniqueMensajeMultimedia() {
		return (MensajeMultimedia) super.uniqueResult();
	}
	
	public MensajeMultimedia[] listMensajeMultimedia() {
		java.util.List list = super.list();
		return (MensajeMultimedia[]) list.toArray(new MensajeMultimedia[list.size()]);
	}
}

