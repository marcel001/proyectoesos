/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TipoMultimediaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression tipo;
	public final CollectionExpression mensaje;
	public final CollectionExpression multimediaDron;
	public final CollectionExpression streamingDron;
	
	public TipoMultimediaDetachedCriteria() {
		super(orm.TipoMultimedia.class, orm.TipoMultimediaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		tipo = new StringExpression("tipo", this.getDetachedCriteria());
		mensaje = new CollectionExpression("ORM_Mensaje", this.getDetachedCriteria());
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this.getDetachedCriteria());
		streamingDron = new CollectionExpression("ORM_StreamingDron", this.getDetachedCriteria());
	}
	
	public TipoMultimediaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.TipoMultimediaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		tipo = new StringExpression("tipo", this.getDetachedCriteria());
		mensaje = new CollectionExpression("ORM_Mensaje", this.getDetachedCriteria());
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this.getDetachedCriteria());
		streamingDron = new CollectionExpression("ORM_StreamingDron", this.getDetachedCriteria());
	}
	
	public MensajeDetachedCriteria createMensajeCriteria() {
		return new MensajeDetachedCriteria(createCriteria("ORM_Mensaje"));
	}
	
	public MultimediaDronDetachedCriteria createMultimediaDronCriteria() {
		return new MultimediaDronDetachedCriteria(createCriteria("ORM_MultimediaDron"));
	}
	
	public StreamingDronDetachedCriteria createStreamingDronCriteria() {
		return new StreamingDronDetachedCriteria(createCriteria("ORM_StreamingDron"));
	}
	
	public TipoMultimedia uniqueTipoMultimedia(PersistentSession session) {
		return (TipoMultimedia) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public TipoMultimedia[] listTipoMultimedia(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (TipoMultimedia[]) list.toArray(new TipoMultimedia[list.size()]);
	}
}

