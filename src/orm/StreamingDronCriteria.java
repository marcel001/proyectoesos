/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class StreamingDronCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression url;
	public final TimestampExpression fecha;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	
	public StreamingDronCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		url = new StringExpression("url", this);
		fecha = new TimestampExpression("fecha", this);
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this);
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this);
	}
	
	public StreamingDronCriteria(PersistentSession session) {
		this(session.createCriteria(StreamingDron.class));
	}
	
	public StreamingDronCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public TipoMultimediaCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaCriteria(createCriteria("tipoMultimedia"));
	}
	
	public StreamingDron uniqueStreamingDron() {
		return (StreamingDron) super.uniqueResult();
	}
	
	public StreamingDron[] listStreamingDron() {
		java.util.List list = super.list();
		return (StreamingDron[]) list.toArray(new StreamingDron[list.size()]);
	}
}

