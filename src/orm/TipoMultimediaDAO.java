/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class TipoMultimediaDAO {
	public static TipoMultimedia loadTipoMultimediaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadTipoMultimediaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia getTipoMultimediaByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getTipoMultimediaByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadTipoMultimediaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia getTipoMultimediaByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getTipoMultimediaByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (TipoMultimedia) session.load(orm.TipoMultimedia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia getTipoMultimediaByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (TipoMultimedia) session.get(orm.TipoMultimedia.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (TipoMultimedia) session.load(orm.TipoMultimedia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia getTipoMultimediaByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (TipoMultimedia) session.get(orm.TipoMultimedia.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipoMultimedia(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryTipoMultimedia(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipoMultimedia(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryTipoMultimedia(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia[] listTipoMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listTipoMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia[] listTipoMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listTipoMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipoMultimedia(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.TipoMultimedia as TipoMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryTipoMultimedia(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.TipoMultimedia as TipoMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("TipoMultimedia", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia[] listTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryTipoMultimedia(session, condition, orderBy);
			return (TipoMultimedia[]) list.toArray(new TipoMultimedia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia[] listTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryTipoMultimedia(session, condition, orderBy, lockMode);
			return (TipoMultimedia[]) list.toArray(new TipoMultimedia[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadTipoMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadTipoMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		TipoMultimedia[] tipoMultimedias = listTipoMultimediaByQuery(session, condition, orderBy);
		if (tipoMultimedias != null && tipoMultimedias.length > 0)
			return tipoMultimedias[0];
		else
			return null;
	}
	
	public static TipoMultimedia loadTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		TipoMultimedia[] tipoMultimedias = listTipoMultimediaByQuery(session, condition, orderBy, lockMode);
		if (tipoMultimedias != null && tipoMultimedias.length > 0)
			return tipoMultimedias[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateTipoMultimediaByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateTipoMultimediaByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipoMultimediaByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateTipoMultimediaByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.TipoMultimedia as TipoMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateTipoMultimediaByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.TipoMultimedia as TipoMultimedia");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("TipoMultimedia", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia createTipoMultimedia() {
		return new orm.TipoMultimedia();
	}
	
	public static boolean save(orm.TipoMultimedia tipoMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().saveObject(tipoMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.TipoMultimedia tipoMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().deleteObject(tipoMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.TipoMultimedia tipoMultimedia)throws PersistentException {
		try {
			orm.Mensaje[] lMensajes = tipoMultimedia.mensaje.toArray();
			for(int i = 0; i < lMensajes.length; i++) {
				lMensajes[i].setTipoMultimedia(null);
			}
			orm.MultimediaDron[] lMultimediaDrons = tipoMultimedia.multimediaDron.toArray();
			for(int i = 0; i < lMultimediaDrons.length; i++) {
				lMultimediaDrons[i].setTipoMultimedia(null);
			}
			orm.StreamingDron[] lStreamingDrons = tipoMultimedia.streamingDron.toArray();
			for(int i = 0; i < lStreamingDrons.length; i++) {
				lStreamingDrons[i].setTipoMultimedia(null);
			}
			return delete(tipoMultimedia);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.TipoMultimedia tipoMultimedia, org.orm.PersistentSession session)throws PersistentException {
		try {
			orm.Mensaje[] lMensajes = tipoMultimedia.mensaje.toArray();
			for(int i = 0; i < lMensajes.length; i++) {
				lMensajes[i].setTipoMultimedia(null);
			}
			orm.MultimediaDron[] lMultimediaDrons = tipoMultimedia.multimediaDron.toArray();
			for(int i = 0; i < lMultimediaDrons.length; i++) {
				lMultimediaDrons[i].setTipoMultimedia(null);
			}
			orm.StreamingDron[] lStreamingDrons = tipoMultimedia.streamingDron.toArray();
			for(int i = 0; i < lStreamingDrons.length; i++) {
				lStreamingDrons[i].setTipoMultimedia(null);
			}
			try {
				session.delete(tipoMultimedia);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.TipoMultimedia tipoMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().refresh(tipoMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.TipoMultimedia tipoMultimedia) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().evict(tipoMultimedia);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static TipoMultimedia loadTipoMultimediaByCriteria(TipoMultimediaCriteria tipoMultimediaCriteria) {
		TipoMultimedia[] tipoMultimedias = listTipoMultimediaByCriteria(tipoMultimediaCriteria);
		if(tipoMultimedias == null || tipoMultimedias.length == 0) {
			return null;
		}
		return tipoMultimedias[0];
	}
	
	public static TipoMultimedia[] listTipoMultimediaByCriteria(TipoMultimediaCriteria tipoMultimediaCriteria) {
		return tipoMultimediaCriteria.listTipoMultimedia();
	}
}
