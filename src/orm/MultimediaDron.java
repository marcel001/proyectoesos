/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class MultimediaDron {
	public MultimediaDron() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_MULTIMEDIADRON_ETIQUETA) {
			return ORM_etiqueta;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_MULTIMEDIADRON_TIPOMULTIMEDIA) {
			this.tipoMultimedia = (orm.TipoMultimedia) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MULTIMEDIADRON_CIUDAD) {
			this.ciudad = (orm.Ciudad) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private Double altitud;
	
	private String longitud;
	
	private String latitud;
	
	private String url;
	
	private String urlReducida;
	
	private java.sql.Timestamp fecha;
	
	private orm.TipoMultimedia tipoMultimedia;
	
	private orm.Ciudad ciudad;
	
	private java.util.Set ORM_etiqueta = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setAltitud(double value) {
		setAltitud(new Double(value));
	}
	
	public void setAltitud(Double value) {
		this.altitud = value;
	}
	
	public Double getAltitud() {
		return altitud;
	}
	
	public void setLongitud(String value) {
		this.longitud = value;
	}
	
	public String getLongitud() {
		return longitud;
	}
	
	public void setLatitud(String value) {
		this.latitud = value;
	}
	
	public String getLatitud() {
		return latitud;
	}
	
	public void setUrl(String value) {
		this.url = value;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrlReducida(String value) {
		this.urlReducida = value;
	}
	
	public String getUrlReducida() {
		return urlReducida;
	}
	
	public void setFecha(java.sql.Timestamp value) {
		this.fecha = value;
	}
	
	public java.sql.Timestamp getFecha() {
		return fecha;
	}
	
	private void setORM_Etiqueta(java.util.Set value) {
		this.ORM_etiqueta = value;
	}
	
	private java.util.Set getORM_Etiqueta() {
		return ORM_etiqueta;
	}
	
	public final orm.EtiquetaSetCollection etiqueta = new orm.EtiquetaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MULTIMEDIADRON_ETIQUETA, orm.ORMConstants.KEY_ETIQUETA_MULTIMEDIADRON, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public void setTipoMultimedia(orm.TipoMultimedia value) {
		if (tipoMultimedia != null) {
			tipoMultimedia.multimediaDron.remove(this);
		}
		if (value != null) {
			value.multimediaDron.add(this);
		}
	}
	
	public orm.TipoMultimedia getTipoMultimedia() {
		return tipoMultimedia;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_TipoMultimedia(orm.TipoMultimedia value) {
		this.tipoMultimedia = value;
	}
	
	private orm.TipoMultimedia getORM_TipoMultimedia() {
		return tipoMultimedia;
	}
	
	public void setCiudad(orm.Ciudad value) {
		if (ciudad != null) {
			ciudad.multimediaDron.remove(this);
		}
		if (value != null) {
			value.multimediaDron.add(this);
		}
	}
	
	public orm.Ciudad getCiudad() {
		return ciudad;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Ciudad(orm.Ciudad value) {
		this.ciudad = value;
	}
	
	private orm.Ciudad getORM_Ciudad() {
		return ciudad;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
