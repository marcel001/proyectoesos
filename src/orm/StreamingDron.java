/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class StreamingDron {
	public StreamingDron() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_STREAMINGDRON_TIPOMULTIMEDIA) {
			this.tipoMultimedia = (orm.TipoMultimedia) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String url;
	
	private java.sql.Timestamp fecha;
	
	private orm.TipoMultimedia tipoMultimedia;
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setUrl(String value) {
		this.url = value;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setFecha(java.sql.Timestamp value) {
		this.fecha = value;
	}
	
	public java.sql.Timestamp getFecha() {
		return fecha;
	}
	
	public void setTipoMultimedia(orm.TipoMultimedia value) {
		if (tipoMultimedia != null) {
			tipoMultimedia.streamingDron.remove(this);
		}
		if (value != null) {
			value.streamingDron.add(this);
		}
	}
	
	public orm.TipoMultimedia getTipoMultimedia() {
		return tipoMultimedia;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_TipoMultimedia(orm.TipoMultimedia value) {
		this.tipoMultimedia = value;
	}
	
	private orm.TipoMultimedia getORM_TipoMultimedia() {
		return tipoMultimedia;
	}
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
