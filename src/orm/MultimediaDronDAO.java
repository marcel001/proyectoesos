/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class MultimediaDronDAO {
	public static MultimediaDron loadMultimediaDronByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMultimediaDronByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron getMultimediaDronByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getMultimediaDronByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMultimediaDronByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron getMultimediaDronByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getMultimediaDronByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (MultimediaDron) session.load(orm.MultimediaDron.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron getMultimediaDronByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (MultimediaDron) session.get(orm.MultimediaDron.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (MultimediaDron) session.load(orm.MultimediaDron.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron getMultimediaDronByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (MultimediaDron) session.get(orm.MultimediaDron.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMultimediaDron(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryMultimediaDron(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMultimediaDron(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryMultimediaDron(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron[] listMultimediaDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listMultimediaDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron[] listMultimediaDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listMultimediaDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMultimediaDron(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MultimediaDron as MultimediaDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryMultimediaDron(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MultimediaDron as MultimediaDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("MultimediaDron", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron[] listMultimediaDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryMultimediaDron(session, condition, orderBy);
			return (MultimediaDron[]) list.toArray(new MultimediaDron[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron[] listMultimediaDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryMultimediaDron(session, condition, orderBy, lockMode);
			return (MultimediaDron[]) list.toArray(new MultimediaDron[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMultimediaDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadMultimediaDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		MultimediaDron[] multimediaDrons = listMultimediaDronByQuery(session, condition, orderBy);
		if (multimediaDrons != null && multimediaDrons.length > 0)
			return multimediaDrons[0];
		else
			return null;
	}
	
	public static MultimediaDron loadMultimediaDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		MultimediaDron[] multimediaDrons = listMultimediaDronByQuery(session, condition, orderBy, lockMode);
		if (multimediaDrons != null && multimediaDrons.length > 0)
			return multimediaDrons[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateMultimediaDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateMultimediaDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultimediaDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateMultimediaDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultimediaDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MultimediaDron as MultimediaDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateMultimediaDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.MultimediaDron as MultimediaDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("MultimediaDron", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron createMultimediaDron() {
		return new orm.MultimediaDron();
	}
	
	public static boolean save(orm.MultimediaDron multimediaDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().saveObject(multimediaDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.MultimediaDron multimediaDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().deleteObject(multimediaDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.MultimediaDron multimediaDron)throws PersistentException {
		try {
			if (multimediaDron.getTipoMultimedia() != null) {
				multimediaDron.getTipoMultimedia().multimediaDron.remove(multimediaDron);
			}
			
			if (multimediaDron.getCiudad() != null) {
				multimediaDron.getCiudad().multimediaDron.remove(multimediaDron);
			}
			
			orm.Etiqueta[] lEtiquetas = multimediaDron.etiqueta.toArray();
			for(int i = 0; i < lEtiquetas.length; i++) {
				lEtiquetas[i].multimediaDron.remove(multimediaDron);
			}
			return delete(multimediaDron);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.MultimediaDron multimediaDron, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (multimediaDron.getTipoMultimedia() != null) {
				multimediaDron.getTipoMultimedia().multimediaDron.remove(multimediaDron);
			}
			
			if (multimediaDron.getCiudad() != null) {
				multimediaDron.getCiudad().multimediaDron.remove(multimediaDron);
			}
			
			orm.Etiqueta[] lEtiquetas = multimediaDron.etiqueta.toArray();
			for(int i = 0; i < lEtiquetas.length; i++) {
				lEtiquetas[i].multimediaDron.remove(multimediaDron);
			}
			try {
				session.delete(multimediaDron);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.MultimediaDron multimediaDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().refresh(multimediaDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.MultimediaDron multimediaDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().evict(multimediaDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static MultimediaDron loadMultimediaDronByCriteria(MultimediaDronCriteria multimediaDronCriteria) {
		MultimediaDron[] multimediaDrons = listMultimediaDronByCriteria(multimediaDronCriteria);
		if(multimediaDrons == null || multimediaDrons.length == 0) {
			return null;
		}
		return multimediaDrons[0];
	}
	
	public static MultimediaDron[] listMultimediaDronByCriteria(MultimediaDronCriteria multimediaDronCriteria) {
		return multimediaDronCriteria.listMultimediaDron();
	}
}
