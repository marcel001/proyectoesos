/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Etiqueta {
	public Etiqueta() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_ETIQUETA_MENSAJEMULTIMEDIA) {
			return ORM_mensajeMultimedia;
		}
		else if (key == orm.ORMConstants.KEY_ETIQUETA_MULTIMEDIADRON) {
			return ORM_multimediaDron;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String nombre;
	
	private java.util.Set ORM_mensajeMultimedia = new java.util.HashSet();
	
	private java.util.Set ORM_multimediaDron = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setNombre(String value) {
		this.nombre = value;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	private void setORM_MensajeMultimedia(java.util.Set value) {
		this.ORM_mensajeMultimedia = value;
	}
	
	private java.util.Set getORM_MensajeMultimedia() {
		return ORM_mensajeMultimedia;
	}
	
	public final orm.MensajeMultimediaSetCollection mensajeMultimedia = new orm.MensajeMultimediaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ETIQUETA_MENSAJEMULTIMEDIA, orm.ORMConstants.KEY_MENSAJEMULTIMEDIA_ETIQUETA, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	private void setORM_MultimediaDron(java.util.Set value) {
		this.ORM_multimediaDron = value;
	}
	
	private java.util.Set getORM_MultimediaDron() {
		return ORM_multimediaDron;
	}
	
	public final orm.MultimediaDronSetCollection multimediaDron = new orm.MultimediaDronSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_ETIQUETA_MULTIMEDIADRON, orm.ORMConstants.KEY_MULTIMEDIADRON_ETIQUETA, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
