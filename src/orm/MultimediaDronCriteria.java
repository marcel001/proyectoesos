/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MultimediaDronCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final DoubleExpression altitud;
	public final StringExpression longitud;
	public final StringExpression latitud;
	public final StringExpression url;
	public final StringExpression urlReducida;
	public final TimestampExpression fecha;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	public final IntegerExpression ciudadId;
	public final AssociationExpression ciudad;
	public final CollectionExpression etiqueta;
	
	public MultimediaDronCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		altitud = new DoubleExpression("altitud", this);
		longitud = new StringExpression("longitud", this);
		latitud = new StringExpression("latitud", this);
		url = new StringExpression("url", this);
		urlReducida = new StringExpression("urlReducida", this);
		fecha = new TimestampExpression("fecha", this);
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this);
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this);
		ciudadId = new IntegerExpression("ciudad.id", this);
		ciudad = new AssociationExpression("ciudad", this);
		etiqueta = new CollectionExpression("ORM_Etiqueta", this);
	}
	
	public MultimediaDronCriteria(PersistentSession session) {
		this(session.createCriteria(MultimediaDron.class));
	}
	
	public MultimediaDronCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public TipoMultimediaCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaCriteria(createCriteria("tipoMultimedia"));
	}
	
	public CiudadCriteria createCiudadCriteria() {
		return new CiudadCriteria(createCriteria("ciudad"));
	}
	
	public EtiquetaCriteria createEtiquetaCriteria() {
		return new EtiquetaCriteria(createCriteria("ORM_Etiqueta"));
	}
	
	public MultimediaDron uniqueMultimediaDron() {
		return (MultimediaDron) super.uniqueResult();
	}
	
	public MultimediaDron[] listMultimediaDron() {
		java.util.List list = super.list();
		return (MultimediaDron[]) list.toArray(new MultimediaDron[list.size()]);
	}
}

