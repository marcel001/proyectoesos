/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class MensajeMultimedia {
	public MensajeMultimedia() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_MENSAJEMULTIMEDIA_ETIQUETA) {
			return ORM_etiqueta;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_MENSAJEMULTIMEDIA_MENSAJE) {
			this.mensaje = (orm.Mensaje) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private String url;
	
	private java.sql.Timestamp fecha;
	
	private orm.Mensaje mensaje;
	
	private java.util.Set ORM_etiqueta = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setUrl(String value) {
		this.url = value;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setFecha(java.sql.Timestamp value) {
		this.fecha = value;
	}
	
	public java.sql.Timestamp getFecha() {
		return fecha;
	}
	
	public void setMensaje(orm.Mensaje value) {
		if (mensaje != null) {
			mensaje.mensajeMultimedia.remove(this);
		}
		if (value != null) {
			value.mensajeMultimedia.add(this);
		}
	}
	
	public orm.Mensaje getMensaje() {
		return mensaje;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Mensaje(orm.Mensaje value) {
		this.mensaje = value;
	}
	
	private orm.Mensaje getORM_Mensaje() {
		return mensaje;
	}
	
	private void setORM_Etiqueta(java.util.Set value) {
		this.ORM_etiqueta = value;
	}
	
	private java.util.Set getORM_Etiqueta() {
		return ORM_etiqueta;
	}
	
	public final orm.EtiquetaSetCollection etiqueta = new orm.EtiquetaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MENSAJEMULTIMEDIA_ETIQUETA, orm.ORMConstants.KEY_ETIQUETA_MENSAJEMULTIMEDIA, orm.ORMConstants.KEY_MUL_MANY_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
