/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class StreamingDronDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression url;
	public final TimestampExpression fecha;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	
	public StreamingDronDetachedCriteria() {
		super(orm.StreamingDron.class, orm.StreamingDronCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
	}
	
	public StreamingDronDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.StreamingDronCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this.getDetachedCriteria());
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this.getDetachedCriteria());
	}
	
	public TipoMultimediaDetachedCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaDetachedCriteria(createCriteria("tipoMultimedia"));
	}
	
	public StreamingDron uniqueStreamingDron(PersistentSession session) {
		return (StreamingDron) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public StreamingDron[] listStreamingDron(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (StreamingDron[]) list.toArray(new StreamingDron[list.size()]);
	}
}

