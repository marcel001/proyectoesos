/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class TipoMultimedia {
	public TipoMultimedia() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_TIPOMULTIMEDIA_MENSAJE) {
			return ORM_mensaje;
		}
		else if (key == orm.ORMConstants.KEY_TIPOMULTIMEDIA_MULTIMEDIADRON) {
			return ORM_multimediaDron;
		}
		else if (key == orm.ORMConstants.KEY_TIPOMULTIMEDIA_STREAMINGDRON) {
			return ORM_streamingDron;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String tipo;
	
	private java.util.Set ORM_mensaje = new java.util.HashSet();
	
	private java.util.Set ORM_multimediaDron = new java.util.HashSet();
	
	private java.util.Set ORM_streamingDron = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setTipo(String value) {
		this.tipo = value;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	private void setORM_Mensaje(java.util.Set value) {
		this.ORM_mensaje = value;
	}
	
	private java.util.Set getORM_Mensaje() {
		return ORM_mensaje;
	}
	
	public final orm.MensajeSetCollection mensaje = new orm.MensajeSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TIPOMULTIMEDIA_MENSAJE, orm.ORMConstants.KEY_MENSAJE_TIPOMULTIMEDIA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_MultimediaDron(java.util.Set value) {
		this.ORM_multimediaDron = value;
	}
	
	private java.util.Set getORM_MultimediaDron() {
		return ORM_multimediaDron;
	}
	
	public final orm.MultimediaDronSetCollection multimediaDron = new orm.MultimediaDronSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TIPOMULTIMEDIA_MULTIMEDIADRON, orm.ORMConstants.KEY_MULTIMEDIADRON_TIPOMULTIMEDIA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	private void setORM_StreamingDron(java.util.Set value) {
		this.ORM_streamingDron = value;
	}
	
	private java.util.Set getORM_StreamingDron() {
		return ORM_streamingDron;
	}
	
	public final orm.StreamingDronSetCollection streamingDron = new orm.StreamingDronSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_TIPOMULTIMEDIA_STREAMINGDRON, orm.ORMConstants.KEY_STREAMINGDRON_TIPOMULTIMEDIA, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
