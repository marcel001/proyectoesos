/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MensajeMultimediaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression url;
	public final TimestampExpression fecha;
	public final IntegerExpression mensajeId;
	public final AssociationExpression mensaje;
	public final CollectionExpression etiqueta;
	
	public MensajeMultimediaDetachedCriteria() {
		super(orm.MensajeMultimedia.class, orm.MensajeMultimediaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		mensajeId = new IntegerExpression("mensaje.id", this.getDetachedCriteria());
		mensaje = new AssociationExpression("mensaje", this.getDetachedCriteria());
		etiqueta = new CollectionExpression("ORM_Etiqueta", this.getDetachedCriteria());
	}
	
	public MensajeMultimediaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.MensajeMultimediaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		url = new StringExpression("url", this.getDetachedCriteria());
		fecha = new TimestampExpression("fecha", this.getDetachedCriteria());
		mensajeId = new IntegerExpression("mensaje.id", this.getDetachedCriteria());
		mensaje = new AssociationExpression("mensaje", this.getDetachedCriteria());
		etiqueta = new CollectionExpression("ORM_Etiqueta", this.getDetachedCriteria());
	}
	
	public MensajeDetachedCriteria createMensajeCriteria() {
		return new MensajeDetachedCriteria(createCriteria("mensaje"));
	}
	
	public EtiquetaDetachedCriteria createEtiquetaCriteria() {
		return new EtiquetaDetachedCriteria(createCriteria("ORM_Etiqueta"));
	}
	
	public MensajeMultimedia uniqueMensajeMultimedia(PersistentSession session) {
		return (MensajeMultimedia) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public MensajeMultimedia[] listMensajeMultimedia(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (MensajeMultimedia[]) list.toArray(new MensajeMultimedia[list.size()]);
	}
}

