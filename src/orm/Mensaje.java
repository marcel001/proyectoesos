/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class Mensaje {
	public Mensaje() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_MENSAJE_MENSAJEMULTIMEDIA) {
			return ORM_mensajeMultimedia;
		}
		
		return null;
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == orm.ORMConstants.KEY_MENSAJE_DATOSCONTACTO) {
			this.datosContacto = (orm.DatosContacto) owner;
		}
		
		else if (key == orm.ORMConstants.KEY_MENSAJE_TIPOMULTIMEDIA) {
			this.tipoMultimedia = (orm.TipoMultimedia) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int id;
	
	private java.sql.Timestamp fecha;
	
	private String latitud;
	
	private String longitud;
	
	private String mensaje;
	
	private orm.DatosContacto datosContacto;
	
	private orm.TipoMultimedia tipoMultimedia;
	
	private java.util.Set ORM_mensajeMultimedia = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setFecha(java.sql.Timestamp value) {
		this.fecha = value;
	}
	
	public java.sql.Timestamp getFecha() {
		return fecha;
	}
	
	public void setLatitud(String value) {
		this.latitud = value;
	}
	
	public String getLatitud() {
		return latitud;
	}
	
	public void setLongitud(String value) {
		this.longitud = value;
	}
	
	public String getLongitud() {
		return longitud;
	}
	
	public void setMensaje(String value) {
		this.mensaje = value;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	
	public void setDatosContacto(orm.DatosContacto value) {
		if (datosContacto != null) {
			datosContacto.mensaje.remove(this);
		}
		if (value != null) {
			value.mensaje.add(this);
		}
	}
	
	public orm.DatosContacto getDatosContacto() {
		return datosContacto;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_DatosContacto(orm.DatosContacto value) {
		this.datosContacto = value;
	}
	
	private orm.DatosContacto getORM_DatosContacto() {
		return datosContacto;
	}
	
	public void setTipoMultimedia(orm.TipoMultimedia value) {
		if (tipoMultimedia != null) {
			tipoMultimedia.mensaje.remove(this);
		}
		if (value != null) {
			value.mensaje.add(this);
		}
	}
	
	public orm.TipoMultimedia getTipoMultimedia() {
		return tipoMultimedia;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_TipoMultimedia(orm.TipoMultimedia value) {
		this.tipoMultimedia = value;
	}
	
	private orm.TipoMultimedia getORM_TipoMultimedia() {
		return tipoMultimedia;
	}
	
	private void setORM_MensajeMultimedia(java.util.Set value) {
		this.ORM_mensajeMultimedia = value;
	}
	
	private java.util.Set getORM_MensajeMultimedia() {
		return ORM_mensajeMultimedia;
	}
	
	public final orm.MensajeMultimediaSetCollection mensajeMultimedia = new orm.MensajeMultimediaSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_MENSAJE_MENSAJEMULTIMEDIA, orm.ORMConstants.KEY_MENSAJEMULTIMEDIA_MENSAJE, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
