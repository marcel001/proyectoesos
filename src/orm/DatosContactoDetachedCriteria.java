/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosContactoDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression rut;
	public final StringExpression celular;
	public final StringExpression nombres;
	public final StringExpression apellidos;
	public final StringExpression cargo;
	public final StringExpression correo;
	public final StringExpression pass;
	public final CollectionExpression mensaje;
	
	public DatosContactoDetachedCriteria() {
		super(orm.DatosContacto.class, orm.DatosContactoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		celular = new StringExpression("celular", this.getDetachedCriteria());
		nombres = new StringExpression("nombres", this.getDetachedCriteria());
		apellidos = new StringExpression("apellidos", this.getDetachedCriteria());
		cargo = new StringExpression("cargo", this.getDetachedCriteria());
		correo = new StringExpression("correo", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		mensaje = new CollectionExpression("ORM_Mensaje", this.getDetachedCriteria());
	}
	
	public DatosContactoDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.DatosContactoCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		rut = new StringExpression("rut", this.getDetachedCriteria());
		celular = new StringExpression("celular", this.getDetachedCriteria());
		nombres = new StringExpression("nombres", this.getDetachedCriteria());
		apellidos = new StringExpression("apellidos", this.getDetachedCriteria());
		cargo = new StringExpression("cargo", this.getDetachedCriteria());
		correo = new StringExpression("correo", this.getDetachedCriteria());
		pass = new StringExpression("pass", this.getDetachedCriteria());
		mensaje = new CollectionExpression("ORM_Mensaje", this.getDetachedCriteria());
	}
	
	public MensajeDetachedCriteria createMensajeCriteria() {
		return new MensajeDetachedCriteria(createCriteria("ORM_Mensaje"));
	}
	
	public DatosContacto uniqueDatosContacto(PersistentSession session) {
		return (DatosContacto) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public DatosContacto[] listDatosContacto(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (DatosContacto[]) list.toArray(new DatosContacto[list.size()]);
	}
}

