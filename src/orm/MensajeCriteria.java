/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class MensajeCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final TimestampExpression fecha;
	public final StringExpression latitud;
	public final StringExpression longitud;
	public final StringExpression mensaje;
	public final IntegerExpression datosContactoId;
	public final AssociationExpression datosContacto;
	public final IntegerExpression tipoMultimediaId;
	public final AssociationExpression tipoMultimedia;
	public final CollectionExpression mensajeMultimedia;
	
	public MensajeCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		fecha = new TimestampExpression("fecha", this);
		latitud = new StringExpression("latitud", this);
		longitud = new StringExpression("longitud", this);
		mensaje = new StringExpression("mensaje", this);
		datosContactoId = new IntegerExpression("datosContacto.id", this);
		datosContacto = new AssociationExpression("datosContacto", this);
		tipoMultimediaId = new IntegerExpression("tipoMultimedia.id", this);
		tipoMultimedia = new AssociationExpression("tipoMultimedia", this);
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this);
	}
	
	public MensajeCriteria(PersistentSession session) {
		this(session.createCriteria(Mensaje.class));
	}
	
	public MensajeCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public DatosContactoCriteria createDatosContactoCriteria() {
		return new DatosContactoCriteria(createCriteria("datosContacto"));
	}
	
	public TipoMultimediaCriteria createTipoMultimediaCriteria() {
		return new TipoMultimediaCriteria(createCriteria("tipoMultimedia"));
	}
	
	public MensajeMultimediaCriteria createMensajeMultimediaCriteria() {
		return new MensajeMultimediaCriteria(createCriteria("ORM_MensajeMultimedia"));
	}
	
	public Mensaje uniqueMensaje() {
		return (Mensaje) super.uniqueResult();
	}
	
	public Mensaje[] listMensaje() {
		java.util.List list = super.list();
		return (Mensaje[]) list.toArray(new Mensaje[list.size()]);
	}
}

