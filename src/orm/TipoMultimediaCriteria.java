/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class TipoMultimediaCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression tipo;
	public final CollectionExpression mensaje;
	public final CollectionExpression multimediaDron;
	public final CollectionExpression streamingDron;
	
	public TipoMultimediaCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		tipo = new StringExpression("tipo", this);
		mensaje = new CollectionExpression("ORM_Mensaje", this);
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this);
		streamingDron = new CollectionExpression("ORM_StreamingDron", this);
	}
	
	public TipoMultimediaCriteria(PersistentSession session) {
		this(session.createCriteria(TipoMultimedia.class));
	}
	
	public TipoMultimediaCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public MensajeCriteria createMensajeCriteria() {
		return new MensajeCriteria(createCriteria("ORM_Mensaje"));
	}
	
	public MultimediaDronCriteria createMultimediaDronCriteria() {
		return new MultimediaDronCriteria(createCriteria("ORM_MultimediaDron"));
	}
	
	public StreamingDronCriteria createStreamingDronCriteria() {
		return new StreamingDronCriteria(createCriteria("ORM_StreamingDron"));
	}
	
	public TipoMultimedia uniqueTipoMultimedia() {
		return (TipoMultimedia) super.uniqueResult();
	}
	
	public TipoMultimedia[] listTipoMultimedia() {
		java.util.List list = super.list();
		return (TipoMultimedia[]) list.toArray(new TipoMultimedia[list.size()]);
	}
}

