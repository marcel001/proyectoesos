/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

public class DatosContacto {
	public DatosContacto() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == orm.ORMConstants.KEY_DATOSCONTACTO_MENSAJE) {
			return ORM_mensaje;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int id;
	
	private String rut;
	
	private String celular;
	
	private String nombres;
	
	private String apellidos;
	
	private String cargo;
	
	private String correo;
	
	private String pass;
	
	private java.util.Set ORM_mensaje = new java.util.HashSet();
	
	private void setId(int value) {
		this.id = value;
	}
	
	public int getId() {
		return id;
	}
	
	public int getORMID() {
		return getId();
	}
	
	public void setRut(String value) {
		this.rut = value;
	}
	
	public String getRut() {
		return rut;
	}
	
	public void setCelular(String value) {
		this.celular = value;
	}
	
	public String getCelular() {
		return celular;
	}
	
	public void setNombres(String value) {
		this.nombres = value;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setApellidos(String value) {
		this.apellidos = value;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public void setCargo(String value) {
		this.cargo = value;
	}
	
	public String getCargo() {
		return cargo;
	}
	
	public void setCorreo(String value) {
		this.correo = value;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public void setPass(String value) {
		this.pass = value;
	}
	
	public String getPass() {
		return pass;
	}
	
	private void setORM_Mensaje(java.util.Set value) {
		this.ORM_mensaje = value;
	}
	
	private java.util.Set getORM_Mensaje() {
		return ORM_mensaje;
	}
	
	public final orm.MensajeSetCollection mensaje = new orm.MensajeSetCollection(this, _ormAdapter, orm.ORMConstants.KEY_DATOSCONTACTO_MENSAJE, orm.ORMConstants.KEY_MENSAJE_DATOSCONTACTO, orm.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getId());
	}
	
}
