/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class EtiquetaDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression id;
	public final StringExpression nombre;
	public final CollectionExpression mensajeMultimedia;
	public final CollectionExpression multimediaDron;
	
	public EtiquetaDetachedCriteria() {
		super(orm.Etiqueta.class, orm.EtiquetaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this.getDetachedCriteria());
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this.getDetachedCriteria());
	}
	
	public EtiquetaDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, orm.EtiquetaCriteria.class);
		id = new IntegerExpression("id", this.getDetachedCriteria());
		nombre = new StringExpression("nombre", this.getDetachedCriteria());
		mensajeMultimedia = new CollectionExpression("ORM_MensajeMultimedia", this.getDetachedCriteria());
		multimediaDron = new CollectionExpression("ORM_MultimediaDron", this.getDetachedCriteria());
	}
	
	public MensajeMultimediaDetachedCriteria createMensajeMultimediaCriteria() {
		return new MensajeMultimediaDetachedCriteria(createCriteria("ORM_MensajeMultimedia"));
	}
	
	public MultimediaDronDetachedCriteria createMultimediaDronCriteria() {
		return new MultimediaDronDetachedCriteria(createCriteria("ORM_MultimediaDron"));
	}
	
	public Etiqueta uniqueEtiqueta(PersistentSession session) {
		return (Etiqueta) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Etiqueta[] listEtiqueta(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Etiqueta[]) list.toArray(new Etiqueta[list.size()]);
	}
}

