/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class StreamingDronDAO {
	public static StreamingDron loadStreamingDronByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadStreamingDronByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron getStreamingDronByORMID(int id) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getStreamingDronByORMID(session, id);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadStreamingDronByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron getStreamingDronByORMID(int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return getStreamingDronByORMID(session, id, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (StreamingDron) session.load(orm.StreamingDron.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron getStreamingDronByORMID(PersistentSession session, int id) throws PersistentException {
		try {
			return (StreamingDron) session.get(orm.StreamingDron.class, new Integer(id));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (StreamingDron) session.load(orm.StreamingDron.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron getStreamingDronByORMID(PersistentSession session, int id, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (StreamingDron) session.get(orm.StreamingDron.class, new Integer(id), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryStreamingDron(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryStreamingDron(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryStreamingDron(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return queryStreamingDron(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron[] listStreamingDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listStreamingDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron[] listStreamingDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return listStreamingDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryStreamingDron(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.StreamingDron as StreamingDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryStreamingDron(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.StreamingDron as StreamingDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("StreamingDron", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron[] listStreamingDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryStreamingDron(session, condition, orderBy);
			return (StreamingDron[]) list.toArray(new StreamingDron[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron[] listStreamingDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryStreamingDron(session, condition, orderBy, lockMode);
			return (StreamingDron[]) list.toArray(new StreamingDron[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadStreamingDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return loadStreamingDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StreamingDron[] streamingDrons = listStreamingDronByQuery(session, condition, orderBy);
		if (streamingDrons != null && streamingDrons.length > 0)
			return streamingDrons[0];
		else
			return null;
	}
	
	public static StreamingDron loadStreamingDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StreamingDron[] streamingDrons = listStreamingDronByQuery(session, condition, orderBy, lockMode);
		if (streamingDrons != null && streamingDrons.length > 0)
			return streamingDrons[0];
		else
			return null;
	}
	
	public static java.util.Iterator iterateStreamingDronByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateStreamingDronByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStreamingDronByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = orm.EsosPersistentManager.instance().getSession();
			return iterateStreamingDronByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStreamingDronByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.StreamingDron as StreamingDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iterateStreamingDronByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From orm.StreamingDron as StreamingDron");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("StreamingDron", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron createStreamingDron() {
		return new orm.StreamingDron();
	}
	
	public static boolean save(orm.StreamingDron streamingDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().saveObject(streamingDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(orm.StreamingDron streamingDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().deleteObject(streamingDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.StreamingDron streamingDron)throws PersistentException {
		try {
			if (streamingDron.getTipoMultimedia() != null) {
				streamingDron.getTipoMultimedia().streamingDron.remove(streamingDron);
			}
			
			return delete(streamingDron);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(orm.StreamingDron streamingDron, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (streamingDron.getTipoMultimedia() != null) {
				streamingDron.getTipoMultimedia().streamingDron.remove(streamingDron);
			}
			
			try {
				session.delete(streamingDron);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(orm.StreamingDron streamingDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().refresh(streamingDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(orm.StreamingDron streamingDron) throws PersistentException {
		try {
			orm.EsosPersistentManager.instance().getSession().evict(streamingDron);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static StreamingDron loadStreamingDronByCriteria(StreamingDronCriteria streamingDronCriteria) {
		StreamingDron[] streamingDrons = listStreamingDronByCriteria(streamingDronCriteria);
		if(streamingDrons == null || streamingDrons.length == 0) {
			return null;
		}
		return streamingDrons[0];
	}
	
	public static StreamingDron[] listStreamingDronByCriteria(StreamingDronCriteria streamingDronCriteria) {
		return streamingDronCriteria.listStreamingDron();
	}
}
