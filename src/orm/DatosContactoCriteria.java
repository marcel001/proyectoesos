/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: Universidad de La Frontera
 * License Type: Academic
 */
package orm;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class DatosContactoCriteria extends AbstractORMCriteria {
	public final IntegerExpression id;
	public final StringExpression rut;
	public final StringExpression celular;
	public final StringExpression nombres;
	public final StringExpression apellidos;
	public final StringExpression cargo;
	public final StringExpression correo;
	public final StringExpression pass;
	public final CollectionExpression mensaje;
	
	public DatosContactoCriteria(Criteria criteria) {
		super(criteria);
		id = new IntegerExpression("id", this);
		rut = new StringExpression("rut", this);
		celular = new StringExpression("celular", this);
		nombres = new StringExpression("nombres", this);
		apellidos = new StringExpression("apellidos", this);
		cargo = new StringExpression("cargo", this);
		correo = new StringExpression("correo", this);
		pass = new StringExpression("pass", this);
		mensaje = new CollectionExpression("ORM_Mensaje", this);
	}
	
	public DatosContactoCriteria(PersistentSession session) {
		this(session.createCriteria(DatosContacto.class));
	}
	
	public DatosContactoCriteria() throws PersistentException {
		this(orm.EsosPersistentManager.instance().getSession());
	}
	
	public MensajeCriteria createMensajeCriteria() {
		return new MensajeCriteria(createCriteria("ORM_Mensaje"));
	}
	
	public DatosContacto uniqueDatosContacto() {
		return (DatosContacto) super.uniqueResult();
	}
	
	public DatosContacto[] listDatosContacto() {
		java.util.List list = super.list();
		return (DatosContacto[]) list.toArray(new DatosContacto[list.size()]);
	}
}

