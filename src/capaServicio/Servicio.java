/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capaServicio;

import capaNegocio.Ciudad;
import capaNegocio.MultimediaDron;
import capaNegocio.TipoMultimedia;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.orm.PersistentException;

/**
 *
 * @author kons
 */
@WebService(serviceName = "Servicio")
public class Servicio {

    /**
     * This is a sample web service operation
     */
	
	
	
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    @WebMethod(operationName = "obtenerListaMultimedia")
    public String obtenerListaMultimedia() throws PersistentException {
        
        MultimediaDron datos= new MultimediaDron();
        
        Gson gson= new GsonBuilder().create();
        String resultado= gson.toJson(datos.obtenerListaMultimedia());

        return resultado;
    }
    
        
    @WebMethod(operationName = "obtenerListaTipoMultimedia")
    public String obtenerListaTipoMultimedia() throws PersistentException {
        
        TipoMultimedia datos= new TipoMultimedia();
        
        Gson gson= new GsonBuilder().create();
        String resultado= gson.toJson(datos.obtenerListaTipoMultimedia());

        return resultado;
    }
    
    @WebMethod(operationName = "obtenerListaCiudad")
    public String obtenerListaCiudad() throws PersistentException {
        
        Ciudad datos= new Ciudad();
        
        Gson gson= new GsonBuilder().create();
        String resultado= gson.toJson(datos.obtenerListaCiudad());

        return resultado;
    }
    
    @WebMethod(operationName = "obtenerListaMultimediaFiltro")
    public String obtenerListaMultimediaFiltro(@WebParam(name = "idTipo") int idTipo,@WebParam(name = "idCiudad") int idCiudad,@WebParam(name = "url") String url) throws PersistentException {
        
        MultimediaDron datos= new MultimediaDron();
        
        Gson gson= new GsonBuilder().create();
        String resultado= gson.toJson(datos.obtenerListaMultimediaFiltro(idTipo, idCiudad,url));

        return resultado;
    }
    
}
