<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>
<%@ page isELIgnored="false" %>
<%@ page import="orm.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ESOS / Información Multimedia</title>
<style>
    table {border-collapse: separate;}
    table td { width: 100px; height: 50px;}
</style>
</head>
<body>

<h1>Información del Contenido Multimedia</h1>

<table>
<tr>
<td>ID</td>
<td>URL</td>
<td>Latitud</td>
<td>Longitud</td>
<td>Fecha / Hora</td>
</tr>
<i:forEach items="${resultado}" var="imultimedia">
	<tr>
	 <td>${imultimedia.id}</td>
	 <td>${imultimedia.url}</td>
	 <td>${imultimedia.latitud}</td>
	 <td>${imultimedia.longitud}</td>
	 <td>${imultimedia.fecha}</td>
	</tr>

</i:forEach>
</table>

</body>
</html>