<%-- 
    Document   : busqueda
    Created on : 23-oct-2016, 22:59:46
    Author     : kons
--%>

<%@page import="capaNegocio.Ciudad"%>
<%@page import="capaNegocio.TipoMultimedia"%>
<%@page import="capaNegocio.MultimediaDron"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="capaServicio.Servicio"%>
<%@ page isELIgnored="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>



    </head>
    <script>
        $(document).on('click','tr td.archivo',function(){
                        var src = $(this).html().split("/");
                        if(src[1]=="imagenes"){
                            $("#tituloMultimedia").html(src[2]);
                            if($("#contenedorImagen").length>0){
                                $("#contenedorImagen").attr('src',src[1]+"/"+src[2]);
                            }else{
                                $("#contenedor").empty();
                             $("#contenedor").append("<img id=\"contenedorImagen\" src=\"imagenes/"+src[2]+"\" class=\"img-rounded\" style=\"width:90%; height:90%; margin: 5% \">");

                            } 
                        }
                            if(src[1]=="videos"){
                            $("#tituloMultimedia").html(src[1]);
                            if($("#contenedorVideo").length>0){
                                $("#contenedorVideo").attr('src',src[1]+"/"+src[2]);
                            }else{
                              $("#contenedor").empty();  
                             $("#contenedor").append("<video id=\"contenedorVideo\" style=\"width:90%; margin: 5%\" controls>"+
                            "<source id=\"contenedorVideo\" src=\"videos/"+src[2]+"\" type=\"video/mp4\">"+
                    "</video>");
                        }
                            }
                            
                            
                            if(src[1]=="audios"){
                            $("#tituloMultimedia").html(src[2]);
                            if($("#contenedorAudio").length>0){
                                $("#contenedorAudio").attr('src',src[1]+"/"+src[2]);
                            }else{
                              $("#contenedor").empty();  
                             $("#contenedor").append("<audio controls id=\"contenedorAudio\" style=\"width:90%; margin: 5%; margin-top: 40%\">"+
                            "<source src=\"audios/"+src[2]+"\" type=\"audio/mpeg\">"+
                        "</audio>");
                        }
                            }});
                     
               
                        
        
       
        
    </script>
    <body>
         <%
            Servicio servicio = new Servicio();
            Gson gsonMultimedia = new Gson();
            MultimediaDron mult[]=null;
            if(request.getAttribute("multimedia")!=null){
               mult = gsonMultimedia.fromJson((String)request.getAttribute("multimedia"), MultimediaDron[].class);

            }else if(request.getParameter("tipo")!=null){
              mult = gsonMultimedia.fromJson(servicio.obtenerListaMultimediaFiltro(Integer.parseInt(request.getParameter("tipo")), 0, ""), MultimediaDron[].class);

            }
            TipoMultimedia tipoMult[] = gsonMultimedia.fromJson(servicio.obtenerListaTipoMultimedia(), TipoMultimedia[].class);
            Ciudad ciu[] = gsonMultimedia.fromJson(servicio.obtenerListaCiudad(), Ciudad[].class);
            %>
       
         <!--navbar-->    

            <br>
        <!--video parte izquierda-->
        <div class="container-fluid">
            <div class="row content">
                <div class="col s9">
                      <h3 id="tituloMultimedia" style="color:black; text-align: center; margin-top: 5px"> </h3>

                    <div id="contenedor" style="background-color: lightgrey; width:100%; height:750px ">
                <!--        <video style="width:90%; margin: 5%; margin-top:1%" controls>
                            <source src="videos/anto_bulla.mp4" type="video/mp4">
                        </video>
                        <audio controls style="width:90%; margin: 5%; margin-top:1%">
                            <source src="horse.ogg" type="audio/ogg">
                            <source src="horse.mp3" type="audio/mpeg">
                        </audio> -->
                    </div>
                </div>
    
                <!--tabla parte izquierda-->
                <div class="col s3">
                <div style="margin:1%">
                    <h4>Filtros</h4>
                    <form action="/ProyectoEsos/MultimediaFiltro" method="post">
                    <div class="row">
                        <div class="col s8 text-center">
                            <input type="date" name="fecha" id="fecha" value="" name="fecha">
                        </div>
                    </div>
                        <br>
                    <%-- <div class="row">
                        <input type="hidden" name="tmultimedia" value="${param.tipo}"/>
                        <div class="col s7 text-center">
                            <select class="form-control" name="ciudad" id="Ciudad" required>
                                <option value="0">Elija ciudad</option>
                                <%for (int i = 0; i < ciu.length; i++) {%>
                                <option value="<%=ciu[i].getId() %>"><%=ciu[i].getNombre() %></option>								  
                                <%}%>
                            </select>
                        </div>
                    </div> --%>
                        <br>
                    <div class="row">
                    <div class="col s8 text-center">
                    <div class="input-group">
                        <input type="text" id="buscar" class="input-field" placeholder="Buscar ..." name="buscador">
                    </div>
                        </div>
                    </div>
                        <div class="row">
                        	<div class="col s2 text-center">
                            <input  type="submit" id="botonFiltrar" class="btn btn-success" value="Filtrar">
                        </div> 
                        </div>
                    </form>
                    <br>  
                    <table align="RIGHT" id="tablaMultimedia" >
                        
                    <%for (int i = 0; i < mult.length; i++) {%>
                                                    
                    <tr ><td class="archivo"><%=mult[i].getUrl()%></td>
                       </tr>								  
                                                           <% } %>
                    </table>
                </div>
                </div>
            </div>
        </div>

            <br>
        <!--footer-->
        
            <br>
            <br>
        </footer>
    </body>
</html>

