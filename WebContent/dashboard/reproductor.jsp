<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reproductor Streaming | E-SOS</title>
<link rel="stylesheet" href="../assets/css/estilo.css" />
	<link type="text/css" rel="stylesheet" href="../assets/css/materialize.min.css"  media="screen,projection"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body id="fondoReproductor">
<nav>
       <div class="nav-wrapper lighten-2">
          <a href="" class="brand-logo"><img id="logoEsos" src="http://esos.cl/images/logobar.png"></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
              <li><a href="#">Evento NN</a></li>
              <li><a href="#">Usuario</a></li>
            </ul>
        </div>
      </nav>
	<div class="nav-wrapper" id="ruta"">
      <div class="col s12">
        <a style="margin-left: 15px" href="../alertas.html" class="breadcrumb">Alertas</a>
        <a href="../board.html" class="breadcrumb">Board</a>
        <a href="../streaming?" class="breadcrumb">Historial Streaming</a>
        <a href="#" class="breadcrumb">Reproductor Streaming</a>
      </div>
    </div>
    <br>
	<div class="container">
		<div class="card">
			<center>
		<!-- obtiene el parametro que se envio desde formulario anterior -->
		<%
			String url = request.getParameter("url");
		%>

		<h5>Reproductor Streaming</h5>
		
		<h6>T�tulo: <%= url %></h6>
		<!-- reproduce el video segun la url(String)  -->
		<object width="800" height="600">
			<embed style="margin-bottom: 40px" src="https://www.youtube.com/v/<%=url%>?autoplay=1"
				type="application/x-shockwave-flash" width="800" height="400">
			</embed>
		</object>
	</center>
		</div>
	</div>
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../assets/js/materialize.min.js"></script>
</body>
</html>