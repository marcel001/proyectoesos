<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="i"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w12ww.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Historial Streaming | E-SOS</title>
<link rel="stylesheet" href="assets/css/estilo.css" />
	<link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css"  media="screen,projection"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body id="fondoHistorial">
<nav>
       <div class="nav-wrapper lighten-2">
          <a href="" class="brand-logo"><img id="logoEsos" src="http://esos.cl/images/logobar.png"></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
              <li><a href="#">Evento NN</a></li>
              <li><a href="#">Usuario</a></li>
            </ul>
        </div>
      </nav>
	<div class="nav-wrapper" id="ruta"">
      <div class="col s12">
        <a style="margin-left: 15px" href="alertas.html" class="breadcrumb">Alertas</a>
        <a href="board.html" class="breadcrumb">Board</a>
        <a href="#" class="breadcrumb">Historial Streaming</a>
      </div>
    </div>
    <br>	
	<!--  Tabla con atributos -->
	<div class="container">
		<div class="card">
		<h4 style="text-align: center">Historial Streaming</h4>
			<table class="bordered highlight centered">
		<thead>
			<tr>
				<th>ID</th>
				<th>URL</th>
				<th>Fecha</th>
			</tr>
		</thead>
		<!-- llena la tabla con los atributos por cada celta -->
		<i:forEach items="${resultado}" var="streamings">
			<tr>
				<td>${streamings.id}</td>
				<td>${streamings.url}</td>
				<td>${streamings.fecha}</td>
				
				<td>
				<!-- agrega un boton a cada fila de la tabla  -->
					<form action="dashboard/reproductor.jsp">
						<!-- campo vacio en cada una de las filas que contiene la url de los videos a reproducir -->
						<input type="hidden" value="${streamings.url}" name="url">
						<button id="boton" class="btn-floating btn-large waves-effect waves-light" type="submit"><i class="material-icons">play_arrow</i></button>
					</form>
				</td>
		</i:forEach>
	</table>
		</div>
	</div>
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/materialize.min.js"></script>
</body>
</html>