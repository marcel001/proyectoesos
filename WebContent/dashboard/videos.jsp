<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	  <title>Videos | E-SOS</title>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	  <link href="css/style.css" rel="stylesheet" type="text/css"  media="all" />
	  <link rel="stylesheet" href="../assets/css/estilo.css"/>
	<link type="text/css" rel="stylesheet" href="../assets/css/materialize.min.css"  media="screen,projection"/>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
	  <script src="lib/jquery-1.10.2.min.js" type="text/javascript"></script>
	   <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	  <body>
	  <nav>
       <div class="nav-wrapper lighten-2">
          <a href="" class="brand-logo"><img id="logoEsos" src="http://esos.cl/images/logobar.png"></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
              <li><a href="#">Evento NN</a></li>
              <li><a href="#">Usuario</a></li>
            </ul>
        </div>
      </nav>
	<div class="nav-wrapper" id="ruta"">
      <div class="col s12">
        <a style="margin-left: 15px" href="../alertas.html" class="breadcrumb">Alertas</a>
        <a href="../board.html" class="breadcrumb">Board</a>
        <a href="#" class="breadcrumb">Videos</a>
      </div>
    </div>
    <br>
		<div id="main">
  			<div>	
  					<div class="row">
		<div class="col s12">
		<div class="modulo">
		<!-- <div class="titulo"><h3>Filtros</h3></div>
		<div class="row" style="padding:4px;">
		<div class="col-md-1"></div>
			<div class="col-md-3">
					
					<select class="form-control">
					  <option value="volvo">Por Alerta</option>
					  <option value="saab">Por Region</option>
					  <option value="mercedes">Por Comuna</option>
					</select>
		
			</div>
			<div class="col-md-3">
		
					<select class="form-control">
					  <option value="volvo">Alerta 1 - Volcan Chillan</option>
					  <option value="saab">Alerta 2</option>
					  <option value="mercedes">Alerta 3</option>
					  <option value="audi">Alerta 4</option>
					</select>
			</div> -->
		</div>
			</div>
		</div>
		</div>
		<div class="row">
		<div class="col s12">
		<div class="modulo">
		<div class="titulo"><h3>Videos</h3></div>
		<div class="row" style="padding:4px;">
			<div class="col s12">
					<jsp:include page="busqueda.jsp">
						<jsp:param value="3" name="tipo"/>
					</jsp:include>
			</div>
		</div>
			</div>
		</div>
		</div>
				</div>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="../assets/js/materialize.min.js"></script>
	  </body>
</html>

