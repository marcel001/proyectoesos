<%-- 
    Document   : video
    Created on : 23-oct-2016, 22:59:35
    Author     : kons
--%>

<%@page import="capaNegocio.MultimediaDron"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="capaServicio.Servicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<script>
        $(document).on('click','tr td.archivo',function(){
                        var src = $(this).html().split("/");
                            if(src[0]=="videos"){
                            $("#tituloMultimedia").html(src[1]);
                            if($("#contenedorVideo").length>0){
                                $("#contenedorVideo").attr('src',src[0]+"/"+src[1]);
                            }else{
                              $("#contenedor").empty();  
                             $("#contenedor").append("<video id=\"contenedorVideo\" style=\"width:90%; margin: 5%\" controls>"+
                            "<source id=\"contenedorVideo\" src=\"videos/"+src[1]+"\" type=\"video/mp4\">"+
                    "</video>");
                        }
                            }
                            
                        
        });
       
        
    </script>
    <body>
        
        
            <%
            Servicio servicio = new Servicio();
            Gson gsonMultimedia = new Gson();
            MultimediaDron mult[] = gsonMultimedia.fromJson(servicio.obtenerListaMultimedia(), MultimediaDron[].class);
            
            %>
        <!--navbar-->    

        <!--video parte izquierda-->
        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-9">
                    <h3 id="tituloMultimedia" style="color:black; text-align: center; margin-top: 5px"> </h3>
                    <div id="contenedor" style="background-color: lightgrey; width:100%; height:750px ">
                        
                    </div>
                </div>
    
                <!--tabla parte izquierda-->
                <div class="col-sm-3">
                    <h4>Videos</h4>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search ...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                    </div>
                    <br>
                    <table align="RIGHT" >
                        
                    <%for (int i = 0; i < mult.length; i++) {%>
                                                    
                    <tr ><td class="archivo"><%=mult[i].getUrl()%></td>
                       </tr>								  
                                                   
                                                           <% } %>
                        
                    </table>
                    <!-- <div class="input-group">
                       <input type="text" class="form-control" placeholder="Search Blog..">
                       <span class="input-group-btn">
                         <button class="btn btn-default" type="button">
                           <span class="glyphicon glyphicon-search"></span>
                         </button>
                       </span>
                     </div>-->
                </div>
            </div>
        </div>

            <br>
        <!--footer-->
        <footer class="container-fluid" style="background-color: #111">
            <br>
          <p>Footer Text</p>
        </footer>
    </body>
</html>

